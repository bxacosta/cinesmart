/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entity.Format;
import entity.Movie;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author admin
 */
@Local
public interface FormatServiceLocal {
    
    public abstract void create(Format format);
    public abstract List<Format> findAll();
    public abstract Format findById(Integer id);
    public abstract Format findByName(String name);
    public abstract List<Format> formatsWithFunctionOfMovieForDate(Movie movie, Date date);
}
