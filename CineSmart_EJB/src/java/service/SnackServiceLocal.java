/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entity.Snack;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author admin
 */
@Local
public interface SnackServiceLocal {
    
    public abstract void create(Snack snack);
    public abstract List<Snack> findAll();
    public abstract Snack findById(Integer id);
    public abstract Snack findByName(String name);
    
}
