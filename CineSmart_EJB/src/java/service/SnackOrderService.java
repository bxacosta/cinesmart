/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entity.Account;
import entity.SnackOrder;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author admin
 */
@Stateless
public class SnackOrderService implements SnackOrderServiceLocal {

    @PersistenceContext(unitName = "CineSmartPU")
    private EntityManager em;

    @Override
    public void create(SnackOrder snackOrder) {
        em.persist(snackOrder);
    }

    @Override
    public List<SnackOrder> findAll() {
        TypedQuery<SnackOrder> query = em.createQuery("SELECT so FROM SnackOrder so ORDER BY so.orderDate", SnackOrder.class);

        return query.getResultList();
    }

    @Override
    public SnackOrder findById(Integer id) {
        TypedQuery<SnackOrder> query = em.createQuery("SELECT so FROM SnackOrder so WHERE so.id = :id", SnackOrder.class);
        query.setParameter("id", id);

        if (query.getResultList().isEmpty()) {
            return null;
        } else {
            return query.getResultList().get(0);
        }
    }

    @Override
    public List<SnackOrder> findByAccount(Account acount) {
        TypedQuery<SnackOrder> query = em.createQuery("SELECT so FROM SnackOrder so WHERE so.account = :account ORDER BY so.orderDate", SnackOrder.class);
        query.setParameter("account", acount);

        return query.getResultList();
    }
}
