/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entity.Hall;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author admin
 */
@Local
public interface HallServiceLocal {
    
    public abstract void create(Hall hall);
    public abstract List<Hall> findAll();
    public abstract Hall findById(Integer id);
    public abstract Hall findByName(String name);
}
