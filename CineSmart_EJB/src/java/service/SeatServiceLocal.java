/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entity.Hall;
import entity.Seat;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author admin
 */
@Local
public interface SeatServiceLocal {
    
    public abstract void create(Seat seat);
    public abstract List<Seat> findAll();
    public abstract Seat findById(Integer id);
    public abstract Seat findByName(String name);
    public abstract List<Seat> findByHall(Hall hall);
}
