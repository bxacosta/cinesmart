/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entity.Ticket;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author admin
 */
@Local
public interface TicketServiceLocal {
    
    public abstract void create(Ticket ticket);
    public abstract List<Ticket> findAll();
    public abstract Ticket findById(Integer id);
    public abstract Ticket findByName(String name);
}
