/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entity.Director;
import entity.Gender;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author admin
 */
@Stateless
public class DirectorService implements DirectorServiceLocal {
    
    @PersistenceContext(unitName = "CineSmartPU")
    private EntityManager em; 
    
    @Override
    public void create(Director director) {
        em.persist(director);
    }

    @Override
    public List<Director> findAll() {
        TypedQuery<Director> query = em.createQuery("SELECT d FROM Director d ORDER BY d.name", Director.class);
        
        return query.getResultList(); 
    }

    @Override
    public Director findById(Integer id) {
        TypedQuery<Director> query = em.createQuery("SELECT d FROM Director d WHERE d.id = :id", Director.class);
        query.setParameter("id", id);
        
        if (query.getResultList().isEmpty()) {
            return null;
        } else {
            return query.getResultList().get(0);
        } 
    }

    @Override
    public Director findByName(String name) {
        TypedQuery<Director> query = em.createQuery("SELECT d FROM Director d WHERE d.name = :name", Director.class);
        query.setParameter("name", name);
        
        if (query.getResultList().isEmpty()) {
            return null;
        } else {
            return query.getResultList().get(0);
        } 
    }

}
