/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entity.FunctionType;
import entity.Movie;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

/**
 *
 * @author admin
 */
@Stateless
public class FunctionTypeService implements FunctionTypeServiceLocal {

    @PersistenceContext(unitName = "CineSmartPU")
    private EntityManager em; 

    @Override
    public void create(FunctionType functionType) {
        em.persist(functionType);
    }

    @Override
    public List<FunctionType> findAll() {
        TypedQuery<FunctionType> query = em.createQuery("SELECT ft FROM FunctionType ft ORDER BY ft.name", FunctionType.class);
        
        return query.getResultList(); 
    }

    @Override
    public FunctionType findById(Integer id) {
        TypedQuery<FunctionType> query = em.createQuery("SELECT ft FROM FunctionType ft WHERE ft.id = :id", FunctionType.class);
        query.setParameter("id", id);
        
        if (query.getResultList().isEmpty()) {
            return null;
        } else {
            return query.getResultList().get(0);
        } 
    }

    @Override
    public FunctionType findByName(String name) {
        TypedQuery<FunctionType> query = em.createQuery("SELECT ft FROM FunctionType ft WHERE ft.name = :name", FunctionType.class);
        query.setParameter("name", name);
        
        if (query.getResultList().isEmpty()) {
            return null;
        } else {
            return query.getResultList().get(0);
        } 
    }

    @Override
    public List<FunctionType> typesWithFunctionToTheMovieForDate(Movie movie, Date date) {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date startDate, endDate;
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        startDate = c.getTime();
        
        c.set(Calendar.HOUR_OF_DAY, 23);
        c.set(Calendar.MINUTE, 59);
        endDate = c.getTime();

        Query query = em.createQuery("SELECT DISTINCT f.type FROM Function f WHERE f.movie = :movie AND f.functionDateTime >= :start AND f.functionDateTime < :end");
        query.setParameter("movie", movie);
        query.setParameter("start", startDate, TemporalType.TIMESTAMP);
        query.setParameter("end", endDate, TemporalType.TIMESTAMP);
        
        return query.getResultList();
    }

}
