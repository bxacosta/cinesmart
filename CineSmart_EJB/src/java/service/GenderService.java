/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entity.Gender;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author admin
 */
@Stateless
public class GenderService implements GenderServiceLocal {
    
    @PersistenceContext(unitName = "CineSmartPU")
    private EntityManager em; 
    
    @Override
    public void create(Gender gender) {
        em.persist(gender);
    }
    
    @Override
    public List<Gender> findAll() {
        TypedQuery<Gender> query = em.createQuery("SELECT g FROM Gender g ORDER BY g.name", Gender.class);
        
        return query.getResultList(); 
    }

    @Override
    public Gender findById(Integer id) {
        TypedQuery<Gender> query = em.createQuery("SELECT g FROM Gender g WHERE g.id = :id", Gender.class);
        query.setParameter("id", id);
        
        if (query.getResultList().isEmpty()) {
            return null;
        } else {
            return query.getResultList().get(0);
        } 
    }

    @Override
    public Gender findByName(String name) {
        TypedQuery<Gender> query = em.createQuery("SELECT g FROM Gender g WHERE g.name = :name", Gender.class);
        query.setParameter("name", name);
        
        if (query.getResultList().isEmpty()) {
            return null;
        } else {
            return query.getResultList().get(0);
        } 
    }

}
