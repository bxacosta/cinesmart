/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entity.FunctionOrderDetail;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author admin
 */
@Stateless
public class FunctionOrderDetailService implements FunctionOrderDetailServiceLocal {

    @PersistenceContext(unitName = "CineSmartPU")
    private EntityManager em; 

    
    @Override
    public void create(FunctionOrderDetail functionOrderDetail) {
        em.persist(functionOrderDetail);
    }

}
