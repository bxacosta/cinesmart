/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entity.Gender;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author admin
 */
@Local
public interface GenderServiceLocal {
    
    public abstract void create(Gender gender);
    public abstract List<Gender> findAll();
    public abstract Gender findById(Integer id);
    public abstract Gender findByName(String name);
}
