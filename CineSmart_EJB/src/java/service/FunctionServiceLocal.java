/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entity.Format;
import entity.Function;
import entity.FunctionType;
import entity.Movie;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author admin
 */
@Local
public interface FunctionServiceLocal {

    public abstract void create(Function function);
    public abstract List<Function> findAll();
    public abstract Function findById(Integer id);
    public abstract List<Function> functionsToTheMovieForDate(Movie movie, Date date);
    public abstract List<Function> functionsToTheMovieWithFormatAndTypeForDate(Movie movie, Format format, FunctionType type, Date date);
}
