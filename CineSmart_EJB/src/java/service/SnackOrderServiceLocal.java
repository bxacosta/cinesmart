/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entity.Account;
import entity.SnackOrder;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author admin
 */
@Local
public interface SnackOrderServiceLocal {
    
    public abstract void create(SnackOrder snackOrder);
    public abstract List<SnackOrder> findAll();
    public abstract SnackOrder findById(Integer id);
    public abstract List<SnackOrder> findByAccount(Account acount);
    
}
