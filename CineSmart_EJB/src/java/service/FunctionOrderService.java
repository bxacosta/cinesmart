/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entity.Account;
import entity.FunctionOrder;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author admin
 */
@Stateless
public class FunctionOrderService implements FunctionOrderServiceLocal {

    @PersistenceContext(unitName = "CineSmartPU")
    private EntityManager em; 

    @Override
    public void create(FunctionOrder functionOrder) {
        em.persist(functionOrder);
    }

    @Override
    public List<FunctionOrder> findAll() {
        TypedQuery<FunctionOrder> query = em.createQuery("SELECT fo FROM FunctionOrder fo ORDER BY fo.orderDate", FunctionOrder.class);

        return query.getResultList();
    }

    @Override
    public FunctionOrder findById(Integer id) {
        TypedQuery<FunctionOrder> query = em.createQuery("SELECT fo FROM FunctionOrder fo WHERE fo.id = :id", FunctionOrder.class);
        query.setParameter("id", id);

        if (query.getResultList().isEmpty()) {
            return null;
        } else {
            return query.getResultList().get(0);
        }
    }

    @Override
    public List<FunctionOrder> findByAccount(Account acount) {
        TypedQuery<FunctionOrder> query = em.createQuery("SELECT fo FROM FunctionOrder fo WHERE fo.account = :account ORDER BY fo.orderDate", FunctionOrder.class);
        query.setParameter("account", acount);

        return query.getResultList();
    }

}
