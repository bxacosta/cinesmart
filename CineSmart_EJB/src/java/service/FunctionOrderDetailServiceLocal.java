/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entity.FunctionOrderDetail;
import javax.ejb.Local;

/**
 *
 * @author admin
 */
@Local
public interface FunctionOrderDetailServiceLocal {
    
    public abstract void create(FunctionOrderDetail functionOrderDetail);
}
