/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entity.Director;
import entity.Function;
import entity.Movie;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

/**
 *
 * @author admin
 */
@Stateless
public class MovieService implements MovieServiceLocal {
    
    @PersistenceContext(unitName = "CineSmartPU")
    private EntityManager em; 
    
    @Override
    public void create(Movie movie) {
        em.persist(movie);
    }

    @Override
    public List<Movie> finAll() {
        TypedQuery<Movie> query = em.createQuery("SELECT m FROM Movie m ORDER BY m.title", Movie.class);
        
        return query.getResultList(); 
    }

    @Override
    public Movie findById(Integer id) {
        TypedQuery<Movie> query = em.createQuery("SELECT m FROM Movie m WHERE m.id = :id", Movie.class);
        query.setParameter("id", id);
        
        if (query.getResultList().isEmpty()) {
            return null;
        } else {
            return query.getResultList().get(0);
        } 
    }

    @Override
    public Movie findByTitle(String title) {
        TypedQuery<Movie> query = em.createQuery("SELECT m FROM Movie m WHERE m.title = :title", Movie.class);
        query.setParameter("title", title);
        
        if (query.getResultList().isEmpty()) {
            return null;
        } else {
            return query.getResultList().get(0);
        } 
    }

    @Override
    public List<Movie> moviesWithFunctionForDate(Date date) {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date startDate, endDate;
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        startDate = c.getTime();
        
        c.set(Calendar.HOUR_OF_DAY, 23);
        c.set(Calendar.MINUTE, 59);
        endDate = c.getTime();
        
        System.out.println("START DATE: " + df.format(startDate));
        System.out.println("END DATE: " + df.format(endDate));

        Query query1 = em.createQuery("SELECT DISTINCT f.movie FROM Function f WHERE f.functionDateTime >= :start AND f.functionDateTime < :end");
        query1.setParameter("start", startDate, TemporalType.TIMESTAMP);
        query1.setParameter("end", endDate, TemporalType.TIMESTAMP);
        
        return query1.getResultList();
    }

    

}
