/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entity.Role;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author admin
 */
@Local
public interface RoleServiceLocal {
    
    public abstract void create(Role r);
    
    public abstract Role findByRoleName(String role);
    
    public abstract List<Role> findAll();
   
}
