/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entity.Hall;
import entity.Seat;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author admin
 */
@Stateless
public class SeatService implements SeatServiceLocal {

    @PersistenceContext(unitName = "CineSmartPU")
    private EntityManager em; 

    @Override
    public void create(Seat seat) {
        em.persist(seat);
    }

    @Override
    public List<Seat> findAll() {
        TypedQuery<Seat> query = em.createQuery("SELECT s FROM Seat s ORDER BY s.name", Seat.class);
        
        return query.getResultList(); 
    }

    @Override
    public Seat findById(Integer id) {
        TypedQuery<Seat> query = em.createQuery("SELECT s FROM Seat s WHERE s.id = :id", Seat.class);
        query.setParameter("id", id);
        
        if (query.getResultList().isEmpty()) {
            return null;
        } else {
            return query.getResultList().get(0);
        }
    }

    @Override
    public Seat findByName(String name) {
        TypedQuery<Seat> query = em.createQuery("SELECT s FROM Seat s WHERE s.name = :name", Seat.class);
        query.setParameter("name", name);
        
        if (query.getResultList().isEmpty()) {
            return null;
        } else {
            return query.getResultList().get(0);
        }
    }

    @Override
    public List<Seat> findByHall(Hall hall) {
        TypedQuery<Seat> query = em.createQuery("SELECT s FROM Seat s WHERE s.hall = :hall", Seat.class);
        query.setParameter("hall", hall);
        
        return query.getResultList();
    }

}
