/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entity.Gender;
import entity.Hall;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author admin
 */
@Stateless
public class HallService implements HallServiceLocal {

   @PersistenceContext(unitName = "CineSmartPU")
    private EntityManager em; 

    @Override
    public void create(Hall hall) {
        em.persist(hall);
    }

    @Override
    public List<Hall> findAll() {
        TypedQuery<Hall> query = em.createQuery("SELECT h FROM Hall h ORDER BY h.name", Hall.class);
        
        return query.getResultList(); 
    }

    @Override
    public Hall findById(Integer id) {
        TypedQuery<Hall> query = em.createQuery("SELECT h FROM Hall h WHERE h.id = :id", Hall.class);
        query.setParameter("id", id);
        
        if (query.getResultList().isEmpty()) {
            return null;
        } else {
            return query.getResultList().get(0);
        } 
    }

    @Override
    public Hall findByName(String name) {
        TypedQuery<Hall> query = em.createQuery("SELECT h FROM Hall h WHERE h.name = :name", Hall.class);
        query.setParameter("name", name);
        
        if (query.getResultList().isEmpty()) {
            return null;
        } else {
            return query.getResultList().get(0);
        } 
    }

}
