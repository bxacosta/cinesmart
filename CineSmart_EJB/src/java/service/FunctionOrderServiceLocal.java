/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entity.Account;
import entity.FunctionOrder;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author admin
 */
@Local
public interface FunctionOrderServiceLocal {
    
    public abstract void create(FunctionOrder functionOrder);
    public abstract List<FunctionOrder> findAll();
    public abstract FunctionOrder findById(Integer id);
    public abstract List<FunctionOrder> findByAccount(Account acount);
    
}
