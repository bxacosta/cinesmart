/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entity.Gender;
import entity.Ticket;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author admin
 */
@Stateless
public class TicketService implements TicketServiceLocal {
    
    @PersistenceContext(unitName = "CineSmartPU")
    private EntityManager em; 

    @Override
    public void create(Ticket ticket) {
        em.persist(ticket);
    }

    @Override
    public List<Ticket> findAll() {
        TypedQuery<Ticket> query = em.createQuery("SELECT t FROM Ticket t ORDER BY t.name", Ticket.class);
        
        return query.getResultList(); 
    }

    @Override
    public Ticket findById(Integer id) {
        TypedQuery<Ticket> query = em.createQuery("SELECT t FROM Ticket t WHERE t.id = :id", Ticket.class);
        query.setParameter("id", id);
        
        if (query.getResultList().isEmpty()) {
            return null;
        } else {
            return query.getResultList().get(0);
        }
    }

    @Override
    public Ticket findByName(String name) {
        TypedQuery<Ticket> query = em.createQuery("SELECT t FROM Ticket t WHERE t.name = :name", Ticket.class);
        query.setParameter("name", name);
        
        if (query.getResultList().isEmpty()) {
            return null;
        } else {
            return query.getResultList().get(0);
        } 
    }

}
