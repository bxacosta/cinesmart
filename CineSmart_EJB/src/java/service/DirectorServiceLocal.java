/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entity.Director;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author admin
 */
@Local
public interface DirectorServiceLocal {
    
    public abstract void create(Director director);
    
    public abstract List<Director> findAll();
    
    public abstract Director findById(Integer id);
    
    public abstract Director findByName(String name);
}
