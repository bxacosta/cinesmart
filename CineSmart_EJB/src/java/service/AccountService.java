/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entity.Account;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

/**
 *
 * @author admin
 */
@Stateless
public class AccountService implements AccountServiceLocal {
    
    @PersistenceContext(unitName = "CineSmartPU")
    private EntityManager em;
    
    @Override
    public void create(Account a) {
        Date d = new Date();
        
        a.setCreateDate(d);
        em.persist(a);
    }


    @Override
    public Account findByUsername(String username) {
        TypedQuery<Account> query = em.createQuery("SELECT a FROM Account a WHERE a.username = :username", Account.class);
        query.setParameter("username", username);
       
        if (query.getResultList().isEmpty()) {
            return null;
        } else {
            return query.getResultList().get(0);
        } 
    }

    @Override
    public Account login(String username, String password) {
        Account a = findByUsername(username);
        
        if (a != null) {
            if (a.getPassword().equals(password)) {
                return a;
            }
        }
        
        return null;
    }

    @Override
    public Account findById(Integer id) {
        TypedQuery<Account> query = em.createQuery("SELECT a FROM Account a WHERE a.id = :id", Account.class);
        query.setParameter("id", id);
       
        if (query.getResultList().isEmpty()) {
            return null;
        } else {
            return query.getResultList().get(0);
        } 
    }

    @Override
    public void edit(Account account) {
        em.merge(account);
    }

    @Override
    public List<Account> findAll() {
        TypedQuery<Account> query = em.createQuery("SELECT a FROM Account a", Account.class);
        
        return query.getResultList(); 
    }


}
