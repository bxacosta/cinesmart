/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entity.Snack;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author admin
 */
@Stateless
public class SnackService implements SnackServiceLocal {

    @PersistenceContext(unitName = "CineSmartPU")
    private EntityManager em; 

    
    @Override
    public void create(Snack snack) {
        em.persist(snack);
    }

    @Override
    public List<Snack> findAll() {
        TypedQuery<Snack> query = em.createQuery("SELECT s FROM Snack s ORDER BY s.name", Snack.class);
        
        return query.getResultList(); 
    }

    @Override
    public Snack findById(Integer id) {
        TypedQuery<Snack> query = em.createQuery("SELECT s FROM Snack s WHERE s.id = :id", Snack.class);
        query.setParameter("id", id);
        
        if (query.getResultList().isEmpty()) {
            return null;
        } else {
            return query.getResultList().get(0);
        }
    }

    @Override
    public Snack findByName(String name) {
        TypedQuery<Snack> query = em.createQuery("SELECT s FROM Snack s WHERE s.name = :name", Snack.class);
        query.setParameter("name", name);
        
        if (query.getResultList().isEmpty()) {
            return null;
        } else {
            return query.getResultList().get(0);
        } 
    }

}
