/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entity.Person;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author admin
 */
@Stateless
public class PersonService implements PersonServiceLocal {

    @PersistenceContext(unitName = "CineSmartPU")
    private EntityManager em;

    @Override
    public void create(Person p) {
        em.persist(p);
    }
    
    

}
