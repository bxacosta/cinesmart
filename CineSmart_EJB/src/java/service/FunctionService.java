/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entity.Format;
import entity.Function;
import entity.FunctionType;
import entity.Movie;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

/**
 *
 * @author admin
 */
@Stateless
public class FunctionService implements FunctionServiceLocal {

    @PersistenceContext(unitName = "CineSmartPU")
    private EntityManager em;

    @Override
    public void create(Function function) {
        em.persist(function);
    }

    @Override
    public List<Function> findAll() {
        TypedQuery<Function> query = em.createQuery("SELECT f FROM Function f", Function.class);

        return query.getResultList();
    }

    @Override
    public List<Function> functionsToTheMovieForDate(Movie movie, Date date) {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date startDate, endDate;
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        startDate = c.getTime();
        
        c.set(Calendar.HOUR_OF_DAY, 23);
        c.set(Calendar.MINUTE, 59);
        endDate = c.getTime();
        
        System.out.println("START DATE: " + df.format(startDate));
        System.out.println("END DATE: " + df.format(endDate));

        Query query = em.createQuery("SELECT f FROM Function f WHERE f.movie = :movie AND f.functionDateTime >= :start AND f.functionDateTime < :end");
        query.setParameter("movie", movie);
        query.setParameter("start", startDate, TemporalType.TIMESTAMP);
        query.setParameter("end", endDate, TemporalType.TIMESTAMP);
        
        return query.getResultList();
    }

    @Override
    public List<Function> functionsToTheMovieWithFormatAndTypeForDate(Movie movie, Format format, FunctionType type, Date date) {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date startDate, endDate;
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        startDate = c.getTime();
        
        c.set(Calendar.HOUR_OF_DAY, 23);
        c.set(Calendar.MINUTE, 59);
        endDate = c.getTime();
        
        System.out.println("START DATE: " + df.format(startDate));
        System.out.println("END DATE: " + df.format(endDate));

        Query query = em.createQuery("SELECT f FROM Function f WHERE f.movie = :movie AND f.format = :format AND f.type = :type AND f.functionDateTime >= :start AND f.functionDateTime < :end");
        query.setParameter("movie", movie);
        query.setParameter("format", format);
        query.setParameter("type", type);
        query.setParameter("start", startDate, TemporalType.TIMESTAMP);
        query.setParameter("end", endDate, TemporalType.TIMESTAMP);
        
        return query.getResultList();
    }

    @Override
    public Function findById(Integer id) {
        TypedQuery<Function> query = em.createQuery("SELECT f FROM Function f WHERE f.id = :id", Function.class);
        query.setParameter("id", id);
        
        if (query.getResultList().isEmpty()) {
            return null;
        } else {
            return query.getResultList().get(0);
        } 
    }
}
