/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entity.Movie;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author admin
 */
@Local
public interface MovieServiceLocal {
    
    public abstract void create(Movie movie);
    
    public abstract Movie findById(Integer id);
    
    public abstract Movie findByTitle(String title);
    
    public abstract List<Movie> finAll();
    
    public abstract List<Movie> moviesWithFunctionForDate(Date date);
}
