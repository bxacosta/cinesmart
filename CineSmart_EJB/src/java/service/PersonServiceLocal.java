/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entity.Person;
import javax.ejb.Local;

/**
 *
 * @author admin
 */
@Local
public interface PersonServiceLocal {
    
    public abstract void create(Person p);
}
