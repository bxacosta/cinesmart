/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entity.FunctionType;
import entity.Movie;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author admin
 */
@Local
public interface FunctionTypeServiceLocal {
    
    public abstract void create(FunctionType functionType);
    public abstract List<FunctionType> findAll();
    public abstract FunctionType findById(Integer id);
    public abstract FunctionType findByName(String name);
    public abstract List<FunctionType> typesWithFunctionToTheMovieForDate(Movie movie, Date date);
}
