/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entity.Account;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author admin
 */
@Local
public interface AccountServiceLocal {
    
    public abstract void create(Account a);
   
    public abstract Account findByUsername(String username);
    
    public abstract Account login(String username, String password);
    
    public abstract Account findById(Integer id);
    
    public abstract void edit(Account account);
    
    public abstract List<Account> findAll();
}
