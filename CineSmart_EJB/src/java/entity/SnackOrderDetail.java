/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author admin
 */
@Entity
@Table(name = "snack_order_detail")
public class SnackOrderDetail implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    
    @Column(name = "amount")
    private Double amount;
    
    @Column(name = "cost")
    private Double cost;
    
    @Column(name = "description")
    private String description;
    
    @Column(name = "subTotal")
    private Double subTotal;
     
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_snack")
    private Snack snack;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_snack_order")
    private SnackOrder snackOrder;
    
    public SnackOrderDetail() {
        amount = 0.0;
        subTotal = 0.0;
    }
    
    public SnackOrderDetail(Snack snack) {
        amount = 1.0;
        cost = snack.getCost();
        subTotal = amount * cost;
        description = snack.getName();
        this.snack = snack;
    }

    public SnackOrderDetail(Integer id, Double amount, Double cost, String description, Double subTotal, Snack snack, SnackOrder snackOrder) {
        this.id = id;
        this.amount = amount;
        this.cost = cost;
        this.description = description;
        this.subTotal = subTotal;
        this.snack = snack;
        this.snackOrder = snackOrder;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(Double subTotal) {
        this.subTotal = subTotal;
    }

    public Snack getSnack() {
        return snack;
    }

    public void setSnack(Snack snack) {
        this.snack = snack;
    }

    public SnackOrder getSnackOrder() {
        return snackOrder;
    }

    public void setSnackOrder(SnackOrder snackOrder) {
        this.snackOrder = snackOrder;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SnackOrderDetail other = (SnackOrderDetail) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

}
