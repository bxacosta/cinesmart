/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author admin
 */
@Entity
@Table(name = "movie")
public class Movie implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    
    @Column(name = "title", nullable = false)
    private String title;
    
    @Column(name = "original_title")
    private String originalTitle;
    
    @Column(name = "runtime")
    private String runtime;
    
    @Column(name = "censure")
    private String censure;
    
    @Column(name = "synopsis", columnDefinition = "TEXT")
    private String synopsis;
    
    @Temporal(TemporalType.DATE)
    @Column(name = "releaseDate")
    private Date releaseDate;
    
    @Column(name = "trailer")
    private String trailer;
    
    @Column(name = "image_name")
    private String imageName;
    
    @Lob
    @Column(name = "image_data")
    private byte[] imageData;
    
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "movie_director", 
            joinColumns = @JoinColumn(name = "id_movie", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "id_director", referencedColumnName = "id")
            )
    private List<Director> directors;
    
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "movie_gender", 
            joinColumns = @JoinColumn(name = "id_movie", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "id_gender", referencedColumnName = "id"))
    private List<Gender> genders;
  
    @OneToMany(mappedBy = "movie", fetch = FetchType.LAZY)
    private List<Function> functions;
    
    public Movie() {}

    public Movie(String title, String originalTitle, String runtime, String censure, String synopsis, Date releaseDate, String trailer, String imageName, byte[] imageData, List<Director> directors, List<Gender> genders) {
        this.title = title;
        this.originalTitle = originalTitle;
        this.runtime = runtime;
        this.censure = censure;
        this.synopsis = synopsis;
        this.releaseDate = releaseDate;
        this.trailer = trailer;
        this.imageName = imageName;
        this.imageData = imageData;
        this.directors = directors;
        this.genders = genders;
    }

    //Set and Get
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOriginalTitle() {
        return originalTitle;
    }

    public void setOriginalTitle(String originalTitle) {
        this.originalTitle = originalTitle;
    }

    public String getRuntime() {
        return runtime;
    }

    public void setRuntime(String runtime) {
        this.runtime = runtime;
    }

    public String getCensure() {
        return censure;
    }

    public void setCensure(String censure) {
        this.censure = censure;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getTrailer() {
        return trailer;
    }

    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }

    public List<Director> getDirectors() {
        return directors;
    }

    public void setDirectors(List<Director> directors) {
        this.directors = directors;
    }

    public List<Gender> getGenders() {
        return genders;
    }

    public void setGenders(List<Gender> genders) {
        this.genders = genders;
    }

    public List<Function> getFunctions() {
        return functions;
    }

    public void setFunctions(List<Function> functions) {
        this.functions = functions;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public byte[] getImageData() {
        return imageData;
    }

    public void setImageData(byte[] imageData) {
        this.imageData = imageData;
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Movie other = (Movie) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Movie{" + "id=" + id + ", title=" + title + '}';
    }
}
