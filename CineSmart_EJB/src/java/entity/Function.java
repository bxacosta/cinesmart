/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author admin
 */
@Entity
@Table(name = "function")
public class Function implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
   
    @Column(name = "function_date_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date functionDateTime;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_hall", nullable = false)
    private Hall hall;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_movie", nullable = false)
    private Movie movie;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_funtion_type", nullable = false)
    private FunctionType type;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_format", nullable = false)
    private Format format;
    
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "function_ticket", 
            joinColumns = @JoinColumn(name = "id_function", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "id_ticket", referencedColumnName = "id"))
    private List<Ticket> tickets;
    
    @OneToMany(mappedBy = "function")
    private List<FunctionOrderDetail> functionOrdersDetail;
    
    public Function(){
    
    }

    public Function(Date functionDateTime, Hall hall, Movie movie, FunctionType type, Format format, List<Ticket> tickets) {
        this.functionDateTime = functionDateTime;
        this.hall = hall;
        this.movie = movie;
        this.type = type;
        this.format = format;
        this.tickets = tickets;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    public Hall getHall() {
        return hall;
    }

    public void setHall(Hall hall) {
        this.hall = hall;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public FunctionType getType() {
        return type;
    }

    public void setType(FunctionType type) {
        this.type = type;
    }

    public Format getFormat() {
        return format;
    }

    public void setFormat(Format format) {
        this.format = format;
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }

    public Date getFunctionDateTime() {
        return functionDateTime;
    }

    public void setFunctionDateTime(Date functionDateTime) {
        this.functionDateTime = functionDateTime;
    }

    public List<FunctionOrderDetail> getFunctionOrdersDetail() {
        return functionOrdersDetail;
    }

    public void setFunctionOrdersDetail(List<FunctionOrderDetail> functionOrdersDetail) {
        this.functionOrdersDetail = functionOrdersDetail;
    }
    
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Function other = (Function) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Function{" + "id=" + id + ", hall=" + hall.getName() + ", movie=" + movie.getTitle() + ", type=" + type.getName() + ", format=" + format.getName() + ", tickets=" + tickets.size() + '}';
    }
    
    
}
