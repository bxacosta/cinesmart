/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

/**
 *
 * @author admin
 */
@Entity
public class Seat implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(name = "numRow")
    private String numRow;
    
    @Column(name = "numColumn")
    private String numColumn;
    
    @Column(name = "type")
    private String type;
    
    @ManyToOne
    @JoinColumn(name = "id_hall")
    private Hall hall;
    
    @ManyToMany(mappedBy = "seats", fetch = FetchType.LAZY)
    private List<FunctionOrderDetail> functionOrdersDetail;
    
    public Seat() {
        
    }

    public Seat(String numRow, String numColumn, String type) {
        this.numRow = numRow;
        this.numColumn = numColumn;
        this.type = type;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumRow() {
        return numRow;
    }

    public void setNumRow(String numRow) {
        this.numRow = numRow;
    }

    public String getNumColumn() {
        return numColumn;
    }

    public void setNumColumn(String numColumn) {
        this.numColumn = numColumn;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Hall getHall() {
        return hall;
    }

    public void setHall(Hall hall) {
        this.hall = hall;
    }

    public List<FunctionOrderDetail> getFunctionOrdersDetail() {
        return functionOrdersDetail;
    }

    public void setFunctionOrdersDetail(List<FunctionOrderDetail> functionOrdersDetail) {
        this.functionOrdersDetail = functionOrdersDetail;
    }
    
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Seat other = (Seat) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
}
