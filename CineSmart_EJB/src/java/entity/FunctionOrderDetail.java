/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author admin
 */
@Entity
@Table(name = "function_order_detail")
public class FunctionOrderDetail implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    
    @Column(name = "amount")
    private Double amount;
    
    @Column(name = "cost")
    private Double cost;
    
    @Column(name = "description")
    private String description;
    
    @Column(name = "subTotal")
    private Double subTotal;
     
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_function")
    private Function function;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_function_order")
    private FunctionOrder functionOrder;
    
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "function_order_detail_seat", 
            joinColumns = @JoinColumn(name = "id_function_order_detail", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "id_seat", referencedColumnName = "id")
            )
    private List<Seat> seats;

    public FunctionOrderDetail() {
        this.amount = 0.0;
        this.subTotal = 0.0;
        seats = new ArrayList<>();
    }

    public FunctionOrderDetail(Double cost, String description, Function function) {
        this.amount = 0.0;
        this.subTotal = 0.0;
        this.cost = cost;
        this.description = description;
        this.function = function;
        seats = new ArrayList<>();
    }
    
    public FunctionOrderDetail(Double amount, Double cost, String description, Double subTotal, Function function, FunctionOrder functionOrder) {
        this.amount = amount;
        this.cost = cost;
        this.description = description;
        this.subTotal = subTotal;
        this.function = function;
        this.functionOrder = functionOrder;
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(Double subTotal) {
        this.subTotal = subTotal;
    }

    public Function getFunction() {
        return function;
    }

    public void setFunction(Function function) {
        this.function = function;
    }

    public FunctionOrder getFunctionOrder() {
        return functionOrder;
    }

    public void setFunctionOrder(FunctionOrder functionOrder) {
        this.functionOrder = functionOrder;
    }

    public List<Seat> getSeats() {
        return seats;
    }

    public void setSeats(List<Seat> seats) {
        this.seats = seats;
    }
    
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FunctionOrderDetail other = (FunctionOrderDetail) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
}
