/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entity.Snack;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import service.SnackServiceLocal;

/**
 *
 * @author admin
 */
@ManagedBean
@RequestScoped
public class ViewSnackController {

    @EJB
    private SnackServiceLocal snackService;
    
    private List<Snack> snacks;
    
    @PostConstruct
    public void init() {
        snacks = snackService.findAll();
    }
    
    //Set y Get
    public List<Snack> getSnacks() {
        return snacks;
    }

    public void setSnacks(List<Snack> snacks) {
        this.snacks = snacks;
    }
}
