/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entity.Format;
import entity.Function;
import entity.FunctionType;
import entity.Movie;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import model.ShowFunctionModel;
import service.FormatServiceLocal;
import service.FunctionServiceLocal;
import service.FunctionTypeServiceLocal;
import service.MovieServiceLocal;

/**
 *
 * @author admin
 */
@ManagedBean
@RequestScoped
public class ShowFunctionController {

    @EJB
    private FunctionServiceLocal functionService;
    @EJB
    private FormatServiceLocal formatService;
    @EJB
    private FunctionTypeServiceLocal functionTypeService;
    
    
    private FacesContext fc = FacesContext.getCurrentInstance();
    private List<ShowFunctionModel> functionsList = new ArrayList<>();
    
    private Movie movie;
    private String preSynopsis;
    private String fullSynopsis;
    private Date date;

    @PostConstruct
    public void init() throws IOException {
        System.out.println("@PostConstruct ShowFunctionController");
        movie = (Movie) fc.getExternalContext().getSessionMap().get("movie");

        date = new Date();

        if (movie == null) {
            fc.getExternalContext().redirect(fc.getExternalContext().getRequestContextPath() + "/index.xhtml");
        } else {
            createSynopsis();
            List<Format> formats = formatService.formatsWithFunctionOfMovieForDate(movie, date);
            List<FunctionType> functionTypes = functionTypeService.typesWithFunctionToTheMovieForDate(movie, date);
            System.out.println("Formats available: " + formats.size());
            System.out.println("Funtion Types available: " + functionTypes.size());
            loadFunctionsList(formats, functionTypes);
        }
    }

    public String selectFunction(Integer id) {
        System.out.println("METHOD selectFunction()");
        System.out.println("Function ID: " + id);

        fc.getExternalContext().getSessionMap().put("function", functionService.findById(id));
        return "select_ticket?faces-redirect=true";
    }

    private void createSynopsis() {
        String synopsis = movie.getSynopsis();
        preSynopsis = "";
        if (synopsis.length() > 300) {
            String[] parts = movie.getSynopsis().split(" ");
            for (int i = 0; i < 20; i++) {
                preSynopsis = preSynopsis + parts[i] + " ";
            }
            fullSynopsis = synopsis.substring(preSynopsis.length());
        } else {
            preSynopsis = synopsis;
            fullSynopsis = null;
        }
    }

    private void loadFunctionsList(List<Format> formats, List<FunctionType> functionTypes) {
        System.out.println("METHOD loadFunctionsList()");
        for (Format format : formats) {
            for (FunctionType functionType : functionTypes) {
                List<Function> functions = functionService.functionsToTheMovieWithFormatAndTypeForDate(movie, format, functionType, date);
                if (!functions.isEmpty()) {
                    System.out.println("Funtions for the format " + format.getName() + " with type " + functionType.getName() + ": " + functions.size());
                    functionsList.add(new ShowFunctionModel(format.getName(), functionType.getName(), functions));
                }
            }
        }
    }

    //Set and Get
    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public String getPreSynopsis() {
        return preSynopsis;
    }

    public void setPreSynopsis(String preSynopsis) {
        this.preSynopsis = preSynopsis;
    }

    public String getFullSynopsis() {
        return fullSynopsis;
    }

    public void setFullSynopsis(String fullSynopsis) {
        this.fullSynopsis = fullSynopsis;
    }

    public List<ShowFunctionModel> getFunctionsList() {
        return functionsList;
    }

    public void setFunctionsList(List<ShowFunctionModel> functionsList) {
        this.functionsList = functionsList;
    }
}
