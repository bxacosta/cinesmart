/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import componet.AccountConverter;
import entity.Account;
import entity.Person;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import model.AccountModel;
import service.AccountServiceLocal;

/**
 *
 * @author admin
 */
@ManagedBean
@RequestScoped
public class ProfileController extends MessageController implements Serializable {

    private FacesContext fc = FacesContext.getCurrentInstance();
    private AccountConverter ac = new AccountConverter();
    private Account currentAccount;
    
    @EJB
    private AccountServiceLocal accountService;
    @ManagedProperty(value = "#{accountModel}")
    private AccountModel account;

    @PostConstruct
    public void init() {
        System.out.println("Mathod init() AccountConverter");
        Account loggedAcount = (Account) fc.getExternalContext().getSessionMap().get("account");

        currentAccount = accountService.findById(loggedAcount.getId());

        account = ac.convertAccountToAccountModel(accountService.findById(currentAccount.getId()));
    }

    public void updateAccount() {
        System.out.println("Method updateAccount()");
        System.out.println("Data from view: " + account.toString());

        Account edited = currentAccount; 

        boolean editedEmail = !currentAccount.getUsername().equals(account.getEmail());
        boolean editedPassword = !account.getNewPassword().isEmpty() && !account.getOldPassword().isEmpty();
        
        boolean error = false;
        
        if (editedEmail) {
            if (accountService.findByUsername(account.getEmail()) == null) {
                edited.setUsername(account.getEmail());
                error = false;
            } else {
                fc.addMessage("error-alert", new FacesMessage("El email que desea registrar ya existe."));
                error = true;
            }
        } else if (editedPassword) {
            if (account.getOldPassword().equals(currentAccount.getPassword()) && !account.getNewPassword().isEmpty()) {
                edited.setPassword(account.getNewPassword());
                error = false;
            } else {
                fc.addMessage("error-alert", new FacesMessage("Los datos ingresados en los campos de contraseña son incorrectos."));
                error = true;
            }
        }
        
        if (!error) {
            edited = ac.convertAccountModelToAccount(account, edited);
            this.accountService.edit(edited);
            fc.getExternalContext().getSessionMap().remove("account");
            fc.getExternalContext().getSessionMap().put("account", edited);
            fc.addMessage("success-alert", new FacesMessage("Los datos de su cuenta se actualizaron correctamente."));
        }
        
    }

    //Set ang Get
    public AccountModel getAccount() {
        return account;
    }

    public void setAccount(AccountModel account) {
        this.account = account;
    }

    @Override
    protected FacesContext getFacesContext() {
        return fc;
    }
}
