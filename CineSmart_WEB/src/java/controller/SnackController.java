/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import componet.AppConstant;
import componet.FileUploadService;
import componet.SnackConverter;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import model.SnackModel;
import org.primefaces.model.UploadedFile;
import service.SnackServiceLocal;

/**
 *
 * @author admin
 */
@ManagedBean
@RequestScoped
public class SnackController extends MessageController implements Serializable {

    @EJB
    private SnackServiceLocal snackService;
    
    @ManagedProperty(value = "#{snackModel}")
    private SnackModel snack;
    
    private FacesContext fc = FacesContext.getCurrentInstance();
    private SnackConverter sc = new SnackConverter();
    private UploadedFile file;

    @PostConstruct
    public void Init() {
    }

    public void saveSnack() {
        System.out.println("METHOD saveSnack");

        try {
            if (snack.getName().isEmpty()) {
                fc.addMessage("error-alert", new FacesMessage("Debe asignar un nombre y un precio al snack para se guardado."));
            } else {
                System.out.println("Data from view: " + snack.toString());
                snackService.create(sc.convertSnackModelToSnack(snack));
                
                fc.addMessage("success-alert", new FacesMessage("El Snack " + snack.getName() + " se ha guardado correctamente"));
                
                fc.getViewRoot().getViewMap().remove("snackModel");
            }
        } catch (Exception e) {
            fc.addMessage("error-alert", new FacesMessage("Ocurrio un error mientras se guardaban los datos."));
        }

    }

    public void uploadImage() {
        if (file != null) {
            System.out.println("Method uploadImage()");
            System.out.println("Image name: " + file.getFileName());

            FileUploadService fus = new FileUploadService();

            ServletContext sContext = (ServletContext) fc.getExternalContext().getContext();
            String path = sContext.getRealPath(File.separator).concat(AppConstant.SNACK_IMAGE_PATH);

            String extension = file.getContentType().split("/")[1];

            try {
                String imageName = fus.createFileFromUploadedFile(file, extension, path);
                snack.setImageName(imageName);
                snack.setImageData(file.getContents());
            } catch (IOException ex) {
                System.out.println("Error al subir el archivo");
            }
        } else {
            fc.addMessage("error-alert", new FacesMessage("Seleccione una imagen para ser cargada."));
        }
    }

    // Set and Get
    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public SnackModel getSnack() {
        return snack;
    }

    public void setSnack(SnackModel snack) {
        this.snack = snack;
    }

    @Override
    protected FacesContext getFacesContext() {
        return fc;
    }
}
