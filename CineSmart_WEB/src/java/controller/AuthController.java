/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import componet.AppConstant;
import entity.Account;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author admin
 */
@ManagedBean
@RequestScoped
public class AuthController implements Serializable{
    
    private FacesContext fc = FacesContext.getCurrentInstance();
    private Account account = (Account) fc.getExternalContext().getSessionMap().get("account");
    
    public void init() {
        System.out.println("Method init() AuthController");
    }
    
    public boolean isAuth() {
        return (account != null);
    }

    public void verifySession() {
        try {
            if (!isAuth()) {
               fc.getExternalContext().redirect(fc.getExternalContext().getRequestContextPath() + "/index.xhtml");
            }
        } catch (Exception e) {}
    }
    
    public String logout() {
        fc.getExternalContext().invalidateSession();
        return "/index?faces-redirect=true";
    }
    
    public boolean isAdmin() {
        if (account != null) {
            return account.getRole().getName().equals(AppConstant.ADMIN_ROLE);
        } else {
            return false;
        }
        
        
    }

    // Set and Get
    public Account getAccount() {
        return account;
    }
}
