/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import componet.FunctionOrderConverter;
import entity.Function;
import entity.FunctionOrder;
import entity.FunctionOrderDetail;
import entity.Hall;
import entity.Seat;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import model.FunctionOrderModel;
import service.FunctionOrderServiceLocal;
import service.SeatServiceLocal;

/**
 *
 * @author admin
 */
@ManagedBean
@RequestScoped
public class SelectSeatController implements Serializable {

    @EJB
    private SeatServiceLocal seatService;
    @EJB
    private FunctionOrderServiceLocal functionOrderService;
    @ManagedProperty(value = "#{functionOrderModel}")
    private FunctionOrderModel functionOrder;
    private FunctionOrderConverter foc = new FunctionOrderConverter();
    private FacesContext fc = FacesContext.getCurrentInstance();
    private List<Seat> seats;
    private Integer idSeat;

    @PostConstruct
    public void init() throws IOException {
        System.out.println("@PostConstruct SelectSeatController");

        functionOrder = (FunctionOrderModel) fc.getExternalContext().getSessionMap().get("function_order");

        if (functionOrder == null) {
            fc.getExternalContext().redirect(fc.getExternalContext().getRequestContextPath() + "/index.xhtml");
        } else {

            Hall hall = functionOrder.getOrdersDetail().get(0).getFunction().getHall();
            System.out.println("HALL: " + hall.getName());
            seats = seatService.findByHall(hall);

            for (FunctionOrderDetail fod : functionOrder.getOrdersDetail()) {
                for (Seat seat : fod.getSeats()) {
                    seats.remove(seat);
                }
            }

            System.out.println("Funtion Order Model Detail Size: " + functionOrder.getOrdersDetail().size());
        }
    }

    public void addSeat(String ticket) {
        System.out.println("METHOD addSeat()");
        System.out.println("Seat ID: " + idSeat);
        System.out.println("Ticket: " + ticket);

        for (FunctionOrderDetail fod : functionOrder.getOrdersDetail()) {
            if (fod.getDescription().equals(ticket)) {

                if (fod.getSeats().size() < fod.getAmount()) {
                    System.out.println("Add Seat to " + fod.getDescription());
                    Seat s = seatService.findById(idSeat);
                    fod.getSeats().add(s);
                    seats.remove(s);
                }

                System.out.println("Seats Size: " + fod.getSeats().size());
                break;
            }
        }
    }

    public void removeSeat(int indexDetail, int indexSeat) {
        System.out.println("METHOD removeSeat()");
        System.out.println("Seat Index Detail: " + indexDetail);
        System.out.println("Seat Index Status: " + indexSeat);
        Seat s = functionOrder.getOrdersDetail().get(indexDetail).getSeats().get(indexSeat);
        functionOrder.getOrdersDetail().get(indexDetail).getSeats().remove(s);
        seats.add(s);
    }

    public String pay() {
        functionOrder.setOrderDate(new Date());
        functionOrder.setStatus("Pagada");

        functionOrderService.create(foc.convertFunctionOrderModelToFunctionOrder(functionOrder));

        fc.getViewRoot().getViewMap().remove("functionOrderModel");
        return "my_order?faces-redirect=true";
    }

    //Set and Get
    public List<Seat> getSeats() {
        return seats;
    }

    public void setSeats(List<Seat> seats) {
        this.seats = seats;
    }

    public Integer getIdSeat() {
        return idSeat;
    }

    public void setIdSeat(Integer idSeat) {
        this.idSeat = idSeat;
    }

    public FunctionOrderModel getFunctionOrder() {
        return functionOrder;
    }

    public void setFunctionOrder(FunctionOrderModel functionOrder) {
        this.functionOrder = functionOrder;
    }
}
