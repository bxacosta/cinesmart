/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entity.Movie;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import service.MovieServiceLocal;

/**
 *
 * @author admin
 */
@ManagedBean
@RequestScoped
public class ViewMovieController {
    
    @EJB
    private MovieServiceLocal movieService;
    
    private List<Movie> movies;
    
    @PostConstruct
    public void init() {
        movies = movieService.finAll();
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }
}
