/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import componet.AppConstant;
import componet.FileUploadService;
import componet.MovieConverter;
import entity.Director;
import entity.Gender;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import model.MovieModel;
import org.primefaces.model.UploadedFile;
import service.DirectorServiceLocal;
import service.GenderServiceLocal;
import service.MovieServiceLocal;

/**
 *
 * @author admin
 */
@ManagedBean
@RequestScoped
public class MovieController extends MessageController implements Serializable {

    @EJB
    private DirectorServiceLocal directorService;
    @EJB
    private GenderServiceLocal genderService;
    @EJB
    private MovieServiceLocal movieService;
    @ManagedProperty(value = "#{movieModel}")
    private MovieModel movie;
    private FacesContext fc = FacesContext.getCurrentInstance();
    private List<Director> directors;
    private List<Gender> genders;
    private UploadedFile file;
    private Director director;
    private Gender gender;

    @PostConstruct
    public void init() {
        System.out.println("Method init() MovieController");
        directors = this.directorService.findAll();
        genders = this.genderService.findAll();
        if (!movie.getDirectors().isEmpty()) {
            for (Director dir : movie.getDirectors()) {
                directors.remove(dir);
            }
        }
        if (!movie.getGenders().isEmpty()) {
            for (Gender gen : movie.getGenders()) {
                genders.remove(gen);
            }
        }


        director = new Director();
        gender = new Gender();
    }

    public void addDirector() {
        System.out.println("Method addDirector()");
        System.out.println("Data from view: id: " + movie.getIdDirector());

        Director d = this.directorService.findById(movie.getIdDirector());
        this.movie.setDirectors(d);

        for (Director dir : movie.getDirectors()) {
            directors.remove(dir);
        }

        System.out.println("Size of list: " + movie.getDirectors().size());
    }

    public void removeDirector(Integer id) {
        System.out.println("Method removeDirector()");
        System.out.println("Data from view: id: " + id);
        Director d = this.directorService.findById(id);
        this.movie.getDirectors().remove(d);
        directors.add(d);
        System.out.println("Size of list: " + movie.getDirectors().size());
    }

    public void addGender() {
        System.out.println("Method addGender()");
        System.out.println("Data from view: id: " + movie.getIdGender());

        Gender g = this.genderService.findById(movie.getIdGender());
        this.movie.setGenders(g);

        for (Gender gen : movie.getGenders()) {
            genders.remove(gen);
        }

        System.out.println("Size of list: " + movie.getGenders().size());
    }

    public void removeGender(Integer id) {
        System.out.println("Method removeGender()");
        System.out.println("Data from view: id: " + id);
        Gender g = this.genderService.findById(id);
        this.movie.getGenders().remove(g);
        genders.add(g);
        System.out.println("Size of list: " + movie.getGenders().size());
    }

    public void saveDirector() {
        try {
            if (director.getName().isEmpty()) {
                System.out.println("METHOD saveDirector(), Name: " + director.getName());
                fc.addMessage("error-alert", new FacesMessage("Debe ingresar un nombre valido para poder guardarlo."));
            } else {
                directorService.create(director);
                directors = this.directorService.findAll();
                fc.addMessage("success-alert", new FacesMessage("El director " + director.getName() + " se ha guardado correctamente"));
            }
        } catch (Exception e) {
            fc.addMessage("error-alert", new FacesMessage("Ocurrio un error mientras se guardaban los datos."));
        }

    }

    public void saveGender() {
        try {
            if (gender.getName().isEmpty()) {
                fc.addMessage("error-alert", new FacesMessage("Debe ingresar un nombre valido para poder guardarlo."));
            } else {
                System.out.println("METHOD saveGender(), Name: " + gender.getName());
                genderService.create(gender);
                genders = this.genderService.findAll();
                fc.addMessage("success-alert", new FacesMessage("El genero " + gender.getName() + " se ha guardado correctamente."));
            }
        } catch (Exception e) {
            fc.addMessage("error-alert", new FacesMessage("Ocurrio un error mientras se guardaban los datos."));
        }

    }

    public void saveMovie() {
        System.out.println("Method removeGender()");
        MovieConverter movieConverter = new MovieConverter();
        try {
            if (movie.getDirectors().isEmpty()) {
                fc.addMessage("error-alert", new FacesMessage("Debe seleccionar por lo menos un director."));
            } else if (movie.getGenders().isEmpty()) {
                fc.addMessage("error-alert", new FacesMessage("Debe seleccionar por lo menos un genero."));
            } else {
                System.out.println("Data from view: " + movie.toString());
                this.movieService.create(movieConverter.convertMovieModelToMovie(movie));

                fc.addMessage("success-alert", new FacesMessage("Los datos ingresados de la pelicula se han guardado correctamente."));

                fc.getViewRoot().getViewMap().remove("movieModel");
            }
        } catch (Exception e) {
            fc.addMessage("error-alert", new FacesMessage("No se pudo guardar los datos ingresados, asegurese de que los datos sean correctos."));
        }
    }

    public void uploadImage() {
        if (file != null) {
            System.out.println("Method uploadImage()");
            System.out.println("Image name: " + file.getFileName());

            FileUploadService fs = new FileUploadService();
            ServletContext sc = (ServletContext) fc.getExternalContext().getContext();
            String path = sc.getRealPath(File.separator).concat(AppConstant.MOVIE_IMAGE_PATH);
            String extension = file.getContentType().split("/")[1];

            try {
                String imageName = fs.createFileFromUploadedFile(file, extension, path);
                movie.setImageName(imageName);
                movie.setImageData(file.getContents());
            } catch (IOException ex) {
                System.out.println("Error al subir el archivo");
            }

        } else {
            fc.addMessage("error-alert", new FacesMessage("Seleccione una imagen para ser cargada."));
        }
    }

    //Set and Get
    public List<Director> getDirectors() {
        return directors;
    }

    public void setDirectors(List<Director> directors) {
        this.directors = directors;
    }

    public List<Gender> getGenders() {
        return genders;
    }

    public void setGenders(List<Gender> genders) {
        this.genders = genders;
    }

    public MovieModel getMovie() {
        return movie;
    }

    public void setMovie(MovieModel movie) {
        this.movie = movie;
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public Director getDirector() {
        return director;
    }

    public void setDirector(Director director) {
        this.director = director;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    @Override
    protected FacesContext getFacesContext() {
        return fc;
    }
}
