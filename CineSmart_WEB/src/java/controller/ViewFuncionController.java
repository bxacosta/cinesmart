/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entity.Function;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import service.FunctionServiceLocal;

/**
 *
 * @author admin
 */
@ManagedBean
@RequestScoped
public class ViewFuncionController {

    @EJB
    private FunctionServiceLocal functionService;
    
    private List<Function> functions;
    
    @PostConstruct
    public void init() {
        functions = functionService.findAll();
    }

    public List<Function> getFunctions() {
        return functions;
    }

    public void setFunctions(List<Function> functions) {
        this.functions = functions;
    }
}
