/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entity.Account;
import entity.FunctionOrder;
import entity.FunctionOrderDetail;
import entity.SnackOrder;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import service.FunctionOrderService;
import service.FunctionOrderServiceLocal;
import service.SnackOrderServiceLocal;

/**
 *
 * @author admin
 */
@ManagedBean
@RequestScoped
public class MyOrderController implements Serializable{

    @EJB
    private FunctionOrderServiceLocal functionOrderService;
    @EJB
    private SnackOrderServiceLocal snackOrderService;
    
    private FacesContext fc = FacesContext.getCurrentInstance();
    
    private List<FunctionOrder> functionOrders;
    private List<SnackOrder> snacOrders;
    
    private FunctionOrder functionOrder;
    private SnackOrder snackOrder;
    
    @PostConstruct
    public void init() {
        System.out.println("@PostConstruct MyOrderController");
        
        Account a = (Account) fc.getExternalContext().getSessionMap().get("account");
        
        functionOrders =  functionOrderService.findByAccount(a);
        snacOrders = snackOrderService.findByAccount(a);
        
        functionOrder = new FunctionOrder();
        snackOrder = new SnackOrder();
        
        System.out.println("Number of Function Orders: " + functionOrders.size());
        System.out.println("Number of Snack Orders: " + snacOrders.size());
    }
    
    public void showDetailFunction(FunctionOrder order) {
        functionOrder = order;
    }
    
    public void showDetailSnack(SnackOrder order) {
        snackOrder = order;
    }
    
    //Set an Get
    public List<FunctionOrder> getFunctionOrders() {
        return functionOrders;
    }

    public void setFunctionOrders(List<FunctionOrder> functionOrders) {
        this.functionOrders = functionOrders;
    }

    public List<SnackOrder> getSnacOrders() {
        return snacOrders;
    }

    public void setSnacOrders(List<SnackOrder> snacOrders) {
        this.snacOrders = snacOrders;
    }

    public FunctionOrder getFunctionOrder() {
        return functionOrder;
    }

    public void setFunctionOrder(FunctionOrder functionOrder) {
        this.functionOrder = functionOrder;
    }

    public SnackOrder getSnackOrder() {
        return snackOrder;
    }

    public void setSnackOrder(SnackOrder snackOrder) {
        this.snackOrder = snackOrder;
    }
}
