/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entity.Hall;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import service.HallServiceLocal;

/**
 *
 * @author admin
 */
@ManagedBean
@RequestScoped
public class ViewHallController {

    @EJB
    private HallServiceLocal hallService;
    
    private List<Hall> hall;
    
    @PostConstruct
    public void init() {
        hall = hallService.findAll();
    }
    
    //Set and Get
    public List<Hall> getHall() {
        return hall;
    }

    public void setHall(List<Hall> hall) {
        this.hall = hall;
    }
}
