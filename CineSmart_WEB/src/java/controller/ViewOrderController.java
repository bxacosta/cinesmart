/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entity.FunctionOrder;
import entity.SnackOrder;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import service.FunctionOrderServiceLocal;
import service.SnackOrderServiceLocal;

/**
 *
 * @author admin
 */
@ManagedBean
@RequestScoped
public class ViewOrderController {

    @EJB
    private FunctionOrderServiceLocal functionOrderService;
    @EJB
    private SnackOrderServiceLocal snackOrderService;
    
    private List<FunctionOrder> functionOrders;
    private List<SnackOrder> snackOrders;
    
    private FunctionOrder functionOrder;
    private SnackOrder snackOrder;
    
    @PostConstruct
    public void init() {
        functionOrders = functionOrderService.findAll();
        snackOrders = snackOrderService.findAll();
        
        functionOrder = new FunctionOrder();
        snackOrder = new SnackOrder();
    }
    
    public void showDetailFunction(FunctionOrder order) {
        functionOrder = order;
    }
    
    public void showDetailSnack(SnackOrder order) {
        System.out.println(order.getId());
        snackOrder = order;
    }

    // Set And Get
    public List<FunctionOrder> getFunctionOrders() {
        return functionOrders;
    }

    public void setFunctionOrders(List<FunctionOrder> functionOrders) {
        this.functionOrders = functionOrders;
    }

    public List<SnackOrder> getSnackOrders() {
        return snackOrders;
    }

    public void setSnackOrders(List<SnackOrder> snackOrders) {
        this.snackOrders = snackOrders;
    }

    public FunctionOrder getFunctionOrder() {
        return functionOrder;
    }

    public void setFunctionOrder(FunctionOrder functionOrder) {
        this.functionOrder = functionOrder;
    }

    public SnackOrder getSnackOrder() {
        return snackOrder;
    }

    public void setSnackOrder(SnackOrder snackOrder) {
        this.snackOrder = snackOrder;
    }
    
}
