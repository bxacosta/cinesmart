/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import componet.AppConstant;
import componet.RegisterConverter;
import entity.Account;
import entity.Movie;
import entity.Person;
import entity.Role;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import model.RegisterModel;
import model.LoginModel;
import service.AccountServiceLocal;
import service.FunctionServiceLocal;
import service.MovieServiceLocal;
import service.RoleServiceLocal;



/**
 *
 * @author admin
 */
@ManagedBean
@RequestScoped
public class IndexController extends MessageController implements Serializable{

    @EJB
    private AccountServiceLocal accountService;
    @EJB
    private RoleServiceLocal roleService;
    @EJB
    private FunctionServiceLocal FuntionService;
    @EJB
    private MovieServiceLocal movieService;
    
    @ManagedProperty(value = "#{registerModel}")
    private RegisterModel register; 
    @ManagedProperty(value = "#{loginModel}")
    private LoginModel login;
    
    private FacesContext fc = FacesContext.getCurrentInstance();
    private RegisterConverter  rc = new RegisterConverter();
    private List<Movie> movies;
    
    @PostConstruct
    public void init() {
        System.out.println("@PostConstruct IndexController");
        movies = movieService.moviesWithFunctionForDate(new Date());
    }
    
    @Override
    protected FacesContext getFacesContext() {
        return fc;
    }
    
    public void createAcount() {
        System.out.println("Method createAcount()");
        System.out.println("Data form view: " + register.toString());
        
        
        if (accountService.findByUsername(register.getEmail()) == null) {
            if (register.getPassword().equals(register.getConfirmPassword())) {
                Account a = rc.convertRegisterModelToAccount(register);
                Person p = rc.convertRegisterModelToPerson(register);
                Role r = roleService.findByRoleName(AppConstant.USER_ROLE);

                a.setEnable(true);
                a.setRole(r);
                a.setPerson(p);
               
                try {
                    accountService.create(a);
                    fc.addMessage("register-form:success-alert", new FacesMessage("Usuario " + register.getEmail() + " creado correctamente."));
                    register = new RegisterModel();
                } catch (Exception e) {
                    fc.addMessage("register-form:error-alert", new FacesMessage("No se pudo realizar la solicitud"));
                }
            } else {
                fc.addMessage("register-form:error-alert", new FacesMessage("Las contraseñas no coinciden."));
            }
        } else {
            fc.addMessage("register-form:error-alert", new FacesMessage("El email que desea registrar ya existe."));
        }  
    }
    
    public String userLogin() {
        System.out.println("Method userLogin()");
        System.out.println("Data from view: " + login.toString());
        
        Account a = accountService.login(login.getUsername().trim(), login.getPassword());
        
        if (a != null) {
            fc.getExternalContext().getSessionMap().put("account", a);
            return "/profile?faces-redirect=true";
        } else {
            fc.addMessage("login-form:login-email", new FacesMessage("Credenciales Incorrectas"));
        }
        return null;
    }
    
    public String showFunctions(Integer id) {
        System.out.println("Method showFunctions()");
        System.out.println("Movie ID:" + id);
        
        fc.getExternalContext().getSessionMap().put("movie", movieService.findById(id));
        return "function?faces-redirect=true";
    }
   
    //Set and Get
    public RegisterModel getRegister() {
        return register;
    }

    public void setRegister(RegisterModel register) {
        this.register = register;
    }

    public LoginModel getLogin() {
        return login;
    }

    public void setLogin(LoginModel login) {
        this.login = login;
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }
}

