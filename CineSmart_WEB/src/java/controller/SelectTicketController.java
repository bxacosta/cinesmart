/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entity.Account;
import entity.Function;
import entity.FunctionOrderDetail;
import entity.Ticket;
import java.io.IOException;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import model.FunctionOrderModel;

/**
 *
 * @author admin
 */
@ManagedBean
@RequestScoped
public class SelectTicketController extends MessageController implements Serializable {

    @ManagedProperty(value = "#{functionOrderModel}")
    private FunctionOrderModel functionOrder;
    
    private FacesContext fc = FacesContext.getCurrentInstance();
    
    private Function function;
    private Account account;

    @PostConstruct
    public void init() throws IOException {
        System.out.println("@PostConstruct SelectTicketController");

        function = (Function) fc.getExternalContext().getSessionMap().get("function");

        if (function == null) {
            fc.getExternalContext().redirect(fc.getExternalContext().getRequestContextPath() + "/index.xhtml");
        } else {
            System.out.println("Function ID: " + function.getId());

            account = (Account) fc.getExternalContext().getSessionMap().get("account");

            if (functionOrder.getOrdersDetail().isEmpty()) {
                for (Ticket ticket : function.getTickets()) {
                    functionOrder.getOrdersDetail().add(new FunctionOrderDetail(ticket.getCost(), ticket.getName(), function));
                }
            }
        }
    }

    public void increaseAmount(FunctionOrderDetail order) {
        System.out.println("METHOD increaseAmount()");

        for (FunctionOrderDetail functionOrderDetail : functionOrder.getOrdersDetail()) {
            if (functionOrderDetail.getDescription().equals(order.getDescription())) {
                functionOrderDetail.setAmount(functionOrderDetail.getAmount() + 1.0);
                functionOrderDetail.setSubTotal(functionOrderDetail.getAmount() * functionOrderDetail.getCost());
            }
        }
        calculeTotal();
    }

    public void decreaseAmount(FunctionOrderDetail order) {
        for (FunctionOrderDetail functionOrderDetail : functionOrder.getOrdersDetail()) {
            if (functionOrderDetail.getDescription().equals(order.getDescription())) {
                if (functionOrderDetail.getAmount() > 0) {
                    functionOrderDetail.setAmount(functionOrderDetail.getAmount() - 1.0);
                    functionOrderDetail.setSubTotal(functionOrderDetail.getAmount() * functionOrderDetail.getCost());
                    calculeTotal();
                    break;
                }
            }
        }
    }

    private void calculeTotal() {
        Double total = 0.0;
        Double ticketsNumber = 0.0;
        for (FunctionOrderDetail functionOrderDetail : functionOrder.getOrdersDetail()) {
            total += functionOrderDetail.getSubTotal();
            ticketsNumber += functionOrderDetail.getAmount();
        }
        functionOrder.setTicketsNumber(ticketsNumber);
        functionOrder.setTotal(total);
    }

    public String continueWithTheOrder() throws IOException {
        if (account == null) {
            fc.addMessage("warning-alert", new FacesMessage("Debe iniciar sesión con su cuenta para continuar con el proceso de compra."));
            return "select_ticket";
        } else {

            for (int i = 0; i < functionOrder.getOrdersDetail().size(); i++) {
                if (functionOrder.getOrdersDetail().get(i).getAmount().equals(0.0)) {
                    System.out.println("CREAR EMPTY ORDER DETAIL WITH DESCRIPTION: " + functionOrder.getOrdersDetail().get(i).getDescription());
                    functionOrder.getOrdersDetail().remove(i);
                    i--;
                }
            }

            if (functionOrder.getOrdersDetail().isEmpty()) {
                fc.addMessage("select-ticket-alert:error-alert", new FacesMessage("Debe seleccionar una cantidad de entradas mayor a cero."));
                for (Ticket ticket : function.getTickets()) {
                    functionOrder.getOrdersDetail().add(new FunctionOrderDetail(ticket.getCost(), ticket.getName(), function));
                }
                return "select_ticket";
            } else {
                functionOrder.setClient(account);
                functionOrder.setHall(functionOrder.getOrdersDetail().get(0).getFunction().getHall().getName());
                functionOrder.setFunctionDate(functionOrder.getOrdersDetail().get(0).getFunction().getFunctionDateTime());
                functionOrder.setMovie(functionOrder.getOrdersDetail().get(0).getFunction().getMovie().getTitle());
                fc.getExternalContext().getSessionMap().put("function_order", functionOrder);
                return "select_seat?faces-redirect=true";
            }

        }

    }

    //Set and Get
    public Function getFunction() {
        return function;
    }

    public void setFunction(Function function) {
        this.function = function;
    }

    public FunctionOrderModel getFunctionOrder() {
        return functionOrder;
    }

    public void setFunctionOrder(FunctionOrderModel functionOrder) {
        this.functionOrder = functionOrder;
    }

    @Override
    protected FacesContext getFacesContext() {
        return fc;
    }
}
