/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import componet.SnackOrderConverter;
import entity.Account;
import entity.Snack;
import entity.SnackOrder;
import entity.SnackOrderDetail;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import model.SnackOrderModel;
import service.SnackOrderServiceLocal;

/**
 *
 * @author admin
 */
@ManagedBean
@RequestScoped
public class SnackOrderController implements Serializable {

    @EJB
    private SnackOrderServiceLocal snackOrderService;
    
    @ManagedProperty(value = "#{snackOrderModel}")
    private SnackOrderModel snackOrder;
    
    private SnackOrderConverter soc = new SnackOrderConverter();
    private FacesContext fc = FacesContext.getCurrentInstance();

    @PostConstruct
    public void init() {
        System.out.println("@PostConstruct OrderSnackController");
    }

    public void increaseAmount(int index) {
        System.out.println("METHOD increaseAmount()");
        System.out.println("Index of Item: " + index);
        snackOrder.getSnackOrdersDetail().get(index).setAmount(snackOrder.getSnackOrdersDetail().get(index).getAmount() + 1.0);
        snackOrder.getSnackOrdersDetail().get(index).setSubTotal(snackOrder.getSnackOrdersDetail().get(index).getAmount() * snackOrder.getSnackOrdersDetail().get(index).getCost());
        calculeTotal();
        System.out.println("Number of Items: " + snackOrder.getSnackOrdersDetail().get(index).getAmount());
    }

    public void decreaseAmount(int index) {
        System.out.println("METHOD decreaseAmount()");
        System.out.println("Index of Item: " + index);
        if (snackOrder.getSnackOrdersDetail().get(index).getAmount() > 1) {
            snackOrder.getSnackOrdersDetail().get(index).setAmount(snackOrder.getSnackOrdersDetail().get(index).getAmount() - 1.0);
            snackOrder.getSnackOrdersDetail().get(index).setSubTotal(snackOrder.getSnackOrdersDetail().get(index).getAmount() * snackOrder.getSnackOrdersDetail().get(index).getCost());
            calculeTotal();
            System.out.println("Amount of Items: " + snackOrder.getSnackOrdersDetail().get(index).getAmount());
        }
    }
    
    private void calculeTotal() {
        Double total = 0.0;
        for (SnackOrderDetail detail : snackOrder.getSnackOrdersDetail()) {
            total += detail.getSubTotal();
        }
        snackOrder.setTotal(total);
    }

    public String removeItem(int index) {
        snackOrder.getSnackOrdersDetail().remove(index);
        if (snackOrder.getSnackOrdersDetail().isEmpty()) {
            calculeTotal();
            return cancelOrder();
        } 
        calculeTotal();
        return null;
    }

    public String cancelOrder() {
        fc.getExternalContext().getSessionMap().remove("snackOrderModel");
        return "index?faces-redirec=true";
    }

    public String pay() {
        Account a = (Account) fc.getExternalContext().getSessionMap().get("account");

        if (a == null) {
            fc.addMessage("warning-alert", new FacesMessage("Debe iniciar sesión con su cuenta para continuar con el proceso de compra."));
            return null;
        } else {
            snackOrder.setClient(a);
            snackOrder.setOrderDate(new Date());
            snackOrder.setStatus("Pagada");
            
            snackOrderService.create(soc.convertSnackOrderModelToSnackOrder(snackOrder));           
            
            fc.getExternalContext().getSessionMap().remove("snackOrderModel");
            
            return "my_order?faces-redirect=true";
        }        
    }

    // Set and Get
    public SnackOrderModel getSnackOrder() {
        return snackOrder;
    }

    public void setSnackOrder(SnackOrderModel snackOrder) {
        this.snackOrder = snackOrder;
    }
    
}
