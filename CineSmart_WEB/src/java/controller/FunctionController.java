/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import componet.FunctionConverter;
import entity.Format;
import entity.Function;
import entity.FunctionType;
import entity.Hall;
import entity.Movie;
import entity.Ticket;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import model.FunctionModel;
import service.FormatServiceLocal;
import service.FunctionServiceLocal;
import service.FunctionTypeServiceLocal;
import service.HallServiceLocal;
import service.MovieServiceLocal;
import service.TicketServiceLocal;

/**
 *
 * @author admin
 */
@ManagedBean
@RequestScoped
public class FunctionController extends MessageController implements Serializable {

    @EJB
    private FunctionServiceLocal functionService;
    @EJB
    private MovieServiceLocal movieService;
    @EJB
    private FunctionTypeServiceLocal functionTypeService;
    @EJB
    private FormatServiceLocal formatService;
    @EJB
    private HallServiceLocal hallService;
    @EJB
    private TicketServiceLocal ticketService;
    @ManagedProperty(value = "#{functionModel}")
    private FunctionModel function;
    private FacesContext fc = FacesContext.getCurrentInstance();
    private FunctionConverter functionConverter = new FunctionConverter();
    private List<Movie> movies;
    private List<Hall> theaters;
    private List<FunctionType> functionTypes;
    private List<Format> formats;
    private List<Ticket> tickets;
    private Ticket ticket;
    private FunctionType type;
    private Format format;

    @PostConstruct
    public void init() {
        movies = this.movieService.finAll();
        theaters = this.hallService.findAll();
        functionTypes = this.functionTypeService.findAll();
        formats = this.formatService.findAll();
        tickets = this.ticketService.findAll();

        if (!function.getTickets().isEmpty()) {
            for (Ticket tic : function.getTickets()) {
                tickets.remove(tic);
            }
        }

        ticket = new Ticket();
        type = new FunctionType();
        format = new Format();
    }

    public void addTicket() {
        System.out.println("Method addTicket()");
        Ticket t = ticketService.findById(function.getIdTicket());
        function.setTickets(t);

        for (Ticket tic : function.getTickets()) {
            tickets.remove(tic);
        }

        System.out.println("Tickets List Size: " + function.getTickets().size());
    }

    public void removeTicket(Integer id) {
        System.out.println("Method removeTicket()");
        System.out.println("Id ticket: " + id);
        Ticket t = ticketService.findById(id);
        function.getTickets().remove(t);
        tickets.add(t);
        System.out.println("Tickets List Size: " + function.getTickets().size());
    }

    public void addTime() {
        System.out.println("Method addTime()");

        if (function.getDate().isEmpty() || function.getTime().isEmpty()) {
            fc.addMessage("error-alert", new FacesMessage("Debe ingresar un fecha y un horario para poder agregarlos."));
        } else {
            System.out.println("Data from view: " + function.getDate() + " " + function.getTime());

            DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            Date d;

            try {
                d = df.parse(function.getDate() + " " + function.getTime());
                function.setSchedule(d);
                System.out.println("Date saved: " + df.format(d));
            } catch (ParseException ex) {
                fc.addMessage("error-alert", new FacesMessage("La fehca que ha ingresado es incorrecta o no tiene el formato especificado."));
            }
        }
    }
    
    public void removeTime(int index) {
        System.out.println("METHOD removeTime()");
        System.out.println("Index: " + index);
        function.getSchedule().remove(index);
    }

    public void saveTicket() {
        if (ticket.getName().isEmpty()) {
            fc.addMessage("error-alert", new FacesMessage("Debe ingresar un nombre y un costo para el ticket."));
        } else {
            ticketService.create(ticket);
            tickets = this.ticketService.findAll();
            fc.addMessage("success-alert", new FacesMessage("Los datos del ticket se han guardado correctamente."));
        }
    }

    public void saveFormat() {
        if (format.getName().isEmpty()) {
            fc.addMessage("error-alert", new FacesMessage("Debe ingresar un nombre para el formato."));
        } else {
            formatService.create(format);
            formats = this.formatService.findAll();
            fc.addMessage("success-alert", new FacesMessage("Los datos del formato se han guardado correctamente."));
        }
    }

    public void saveType() {
        if (type.getName().isEmpty()) {
            fc.addMessage("error-alert", new FacesMessage("Debe ingresar un nombre para el tipo."));
        } else {
            functionTypeService.create(type);
            functionTypes = this.functionTypeService.findAll();
            fc.addMessage("success-alert", new FacesMessage("Los datos del tipo se han guardado correctamente."));
        }
    }

    public void saveFunction() {
        System.out.println("Method saveFunction()");

        try {
            if (function.getTickets().isEmpty()) {
                fc.addMessage("error-alert", new FacesMessage("Debe asignar por lo menos un ticket."));
            } else if (function.getSchedule().isEmpty()) {
                fc.addMessage("error-alert", new FacesMessage("Debe asignar por lo menos un horario."));
            } else {
                function.setHall(hallService.findById(function.getIdHall()));
                function.setMovie(movieService.findById(function.getIdMovie()));
                function.setFormat(formatService.findById(function.getIdFormat()));
                function.setType(functionTypeService.findById(function.getIdFunctionType()));
                System.out.println("Data from view: " + function.toString());

                for (Date d : function.getSchedule()) {
                    functionService.create(functionConverter.convertFunctionModelToFunction(function, d));
                }

                fc.addMessage("success-alert", new FacesMessage("Los datos ingresados de la función se han guardado correctamente."));
                fc.getViewRoot().getViewMap().remove("functionModel");
            }
        } catch (Exception e) {
            fc.addMessage("error-alert", new FacesMessage("No se pudo guardar los datos ingresados, asegurese de que los datos sean correctos."));
        }

    }

    // Set and Get
    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }

    public List<Hall> getTheaters() {
        return theaters;
    }

    public void setTheaters(List<Hall> theaters) {
        this.theaters = theaters;
    }

    public List<FunctionType> getFunctionTypes() {
        return functionTypes;
    }

    public void setFunctionTypes(List<FunctionType> functionTypes) {
        this.functionTypes = functionTypes;
    }

    public List<Format> getFormats() {
        return formats;
    }

    public void setFormats(List<Format> formats) {
        this.formats = formats;
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }

    public FunctionModel getFunction() {
        return function;
    }

    public void setFunction(FunctionModel function) {
        this.function = function;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public FunctionType getType() {
        return type;
    }

    public void setType(FunctionType type) {
        this.type = type;
    }

    public Format getFormat() {
        return format;
    }

    public void setFormat(Format format) {
        this.format = format;
    }

    @Override
    protected FacesContext getFacesContext() {
        return fc;
    }
}
