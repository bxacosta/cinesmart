/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entity.Snack;
import entity.SnackOrderDetail;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import model.SnackOrderModel;
import service.SnackServiceLocal;

/**
 *
 * @author admin
 */
@ManagedBean
@RequestScoped
public class ShowSnackController {

    @EJB
    private SnackServiceLocal snackService;
    
    @ManagedProperty(value = "#{snackOrderModel}")
    private SnackOrderModel snackOrder;
    
    private List<Snack> snacks;

    @PostConstruct
    public void init() {
        System.out.println("@PostConstruct ShowSnackController");
        snacks = snackService.findAll();
    }

    public void addSnack(Snack snack) {
        System.out.println("METHOD addSnack");
        System.out.println("Snack: " + snack.getName());


        int index = -1;

        if (!snackOrder.getSnackOrdersDetail().isEmpty()) {
            for (int i = 0; i < snackOrder.getSnackOrdersDetail().size(); i++) {
                if (snackOrder.getSnackOrdersDetail().get(i).getSnack().equals(snack)) {
                    index = i;
                    break;
                }
            }
        }


        if (index >= 0) {
            snackOrder.getSnackOrdersDetail().get(index).setAmount(snackOrder.getSnackOrdersDetail().get(index).getAmount() + 1.0);
            snackOrder.getSnackOrdersDetail().get(index).setCost(snack.getCost());
            snackOrder.getSnackOrdersDetail().get(index).setDescription(snack.getName());
            snackOrder.getSnackOrdersDetail().get(index).setSubTotal(
                    snackOrder.getSnackOrdersDetail().get(index).getAmount() * snackOrder.getSnackOrdersDetail().get(index).getCost());
            calculeTotal();
        } else {
            snackOrder.getSnackOrdersDetail().add(new SnackOrderDetail(snack));
            calculeTotal();
        }

        System.out.println("Snack Order Detail Size: " + snackOrder.getSnackOrdersDetail().size());
    }

    private void calculeTotal() {
        Double total = 0.0;
        
        for (SnackOrderDetail detail : snackOrder.getSnackOrdersDetail()) {
            total += detail.getSubTotal();
        }
        
        snackOrder.setTotal(total);
    }

    // Set and Get
    public List<Snack> getSnacks() {
        return snacks;
    }

    public void setSnacks(List<Snack> snacks) {
        this.snacks = snacks;
    }

    public SnackOrderModel getSnackOrder() {
        return snackOrder;
    }

    public void setSnackOrder(SnackOrderModel snackOrder) {
        this.snackOrder = snackOrder;
    }
}
