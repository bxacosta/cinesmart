/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import javax.faces.context.FacesContext;

/**
 *
 * @author admin
 */
public abstract class MessageController {

    protected abstract FacesContext getFacesContext();
    
    public boolean haveMessage(String clientId) {
        System.out.println("Method haveMessages()");
        return getFacesContext().getMessageList(clientId).size() > 0;
    }
}
