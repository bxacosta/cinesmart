/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import entity.Format;
import entity.FunctionType;
import entity.Hall;
import entity.Movie;
import entity.Ticket;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author admin
 */
@ManagedBean
@ViewScoped
public class FunctionModel implements Serializable{

    private Integer id;
    private Hall hall;
    private Movie movie;
    private FunctionType type;
    private Format format;
    private String time;
    private String date;
    private Integer idMovie;
    private Integer idHall;
    private Integer idFunctionType;
    private Integer idFormat;
    private Integer idTicket;
    private List<Date> schedule;
    private List<Ticket> tickets;

    public FunctionModel() {
        schedule = new ArrayList<>();
        tickets = new ArrayList<>();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Hall getHall() {
        return hall;
    }

    public void setHall(Hall hall) {
        this.hall = hall;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public FunctionType getType() {
        return type;
    }

    public void setType(FunctionType type) {
        this.type = type;
    }

    public Format getFormat() {
        return format;
    }

    public void setFormat(Format format) {
        this.format = format;
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(Ticket ticket) {
        this.tickets.add(ticket);
    }

    public List<Date> getSchedule() {
        return schedule;
    }

    public void setSchedule(Date time) {
        this.schedule.add(time);
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getIdMovie() {
        return idMovie;
    }

    public void setIdMovie(Integer idMovie) {
        this.idMovie = idMovie;
    }

    public Integer getIdHall() {
        return idHall;
    }

    public void setIdHall(Integer idHall) {
        this.idHall = idHall;
    }

    public Integer getIdFunctionType() {
        return idFunctionType;
    }

    public void setIdFunctionType(Integer idFunctionType) {
        this.idFunctionType = idFunctionType;
    }

    public Integer getIdFormat() {
        return idFormat;
    }

    public void setIdFormat(Integer idFormat) {
        this.idFormat = idFormat;
    }

    public Integer getIdTicket() {
        return idTicket;
    }

    public void setIdTicket(Integer idTicket) {
        this.idTicket = idTicket;
    }

    @Override
    public String toString() {
        return "FunctionModel{" + "id=" + id + ", schedule=" + schedule.size() + ", tickets=" + tickets.size() + ", hall=" + hall.getName() + ", movie=" + movie.getTitle() + ", type=" + type.getName() + ", format=" + format.getName() + '}';
    }
     
}
