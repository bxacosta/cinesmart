/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import entity.Director;
import entity.Gender;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author admin
 */
@ManagedBean
@ViewScoped
public class MovieModel implements Serializable{
    
    private Integer id;
    private String title;
    private String originalTitle;
    private String runtime;
    private String censure;
    private String synopsis;
    private Date releaseDate;
    private String trailer;
    private String imageName;
    private byte[] imageData;
    private Integer idDirector;
    private Integer idGender;
    
    private List<Director> directors;
    private List<Gender> genders;
    
    public MovieModel() {
        directors = new ArrayList<>();
        genders =  new ArrayList<>();
    }

    public MovieModel(String title, String originalTitle, String runtime, String censure, String synopsis, Date releaseDate, String trailer, String imageName, byte[] imageData, List<Director> directors, List<Gender> genders) {
        this.title = title;
        this.originalTitle = originalTitle;
        this.runtime = runtime;
        this.censure = censure;
        this.synopsis = synopsis;
        this.releaseDate = releaseDate;
        this.trailer = trailer;
        this.imageName = imageName;
        this.imageData = imageData;
        this.directors = directors;
        this.genders = genders;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOriginalTitle() {
        return originalTitle;
    }

    public void setOriginalTitle(String originalTitle) {
        this.originalTitle = originalTitle;
    }

    public String getRuntime() {
        return runtime;
    }

    public void setRuntime(String runtime) {
        this.runtime = runtime;
    }

    public String getCensure() {
        return censure;
    }

    public void setCensure(String censure) {
        this.censure = censure;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getTrailer() {
        return trailer;
    }

    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }

    public List<Director> getDirectors() {
        return directors;
    }

    public void setDirectors(Director director) {
        this.directors.add(director);
    }

    public List<Gender> getGenders() {
        return genders;
    }

    public void setGenders(Gender gender) {
        this.genders.add(gender);
    }

    public Integer getIdDirector() {
        return idDirector;
    }

    public void setIdDirector(Integer idDirector) {
        this.idDirector = idDirector;
    }

    public Integer getIdGender() {
        return idGender;
    }

    public void setIdGender(Integer idGender) {
        this.idGender = idGender;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public byte[] getImageData() {
        return imageData;
    }

    public void setImageData(byte[] imageData) {
        this.imageData = imageData;
    }
    
    @Override
    public String toString() {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        return "MovieModel{" + "id=" + id + ", title=" + title + ", originalTitle=" + originalTitle + ", runtime=" + runtime + ", censure=" + censure + ", synopsis=" + synopsis + ", releaseDate=" + df.format(releaseDate) + ", trailer=" + trailer + ", image=" + imageName + ", directors=" + directors.size() + ", genders=" + genders.size() + '}';
    }
}
