/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author admin
 */
@ManagedBean
@ViewScoped
public class SnackModel implements Serializable{

    private Integer id;
    private String name;
    private Double cost;
    private String description;
    private String imageName;
    private byte[] imageData;

    public SnackModel() {
    }

    public SnackModel(String name, Double cost, String description, String imageName, byte[] imageData) {
        this.name = name;
        this.cost = cost;
        this.description = description;
        this.imageName = imageName;
        this.imageData = imageData;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public byte[] getImageData() {
        return imageData;
    }

    public void setImageData(byte[] imageData) {
        this.imageData = imageData;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    @Override
    public String toString() {
        return "SnackModel{" + "id=" + id + ", name=" + name + ", cost=" + cost + ", description=" + description + ", imageName=" + imageName + '}';
    }
    
    
}
