/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import entity.Account;
import entity.FunctionOrderDetail;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author admin
 */
@ManagedBean
@ViewScoped
public class FunctionOrderModel implements Serializable{

    private Integer id;
    private String status;
    private Double total;
    private Date orderDate;
    private Account client;
    private Double ticketsNumber;
    private String hall;
    private Date functionDate; 
    private String movie;
    private List<FunctionOrderDetail> ordersDetail;

    public FunctionOrderModel() {
        total = 0.0;
        ordersDetail = new ArrayList<>();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public Account getClient() {
        return client;
    }

    public void setClient(Account client) {
        this.client = client;
    }

    public List<FunctionOrderDetail> getOrdersDetail() {
        return ordersDetail;
    }

    public void setOrdersDetail(List<FunctionOrderDetail> ordersDetail) {
        this.ordersDetail = ordersDetail;
    }

    public Double getTicketsNumber() {
        return ticketsNumber;
    }

    public void setTicketsNumber(Double ticketsNumber) {
        this.ticketsNumber = ticketsNumber;
    }

    public String getHall() {
        return hall;
    }

    public void setHall(String hall) {
        this.hall = hall;
    }

    public Date getFunctionDate() {
        return functionDate;
    }

    public void setFunctionDate(Date functionDate) {
        this.functionDate = functionDate;
    }

    public String getMovie() {
        return movie;
    }

    public void setMovie(String movie) {
        this.movie = movie;
    }
}
