/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import entity.Person;
import entity.Role;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author admin
 */
@ManagedBean
@RequestScoped
public class AccountModel implements Serializable{

    private Integer id;
    private String email;
    private String oldPassword;
    private String newPassword;
    private boolean enable;
    private Person person;
    private Role role;
    
    public AccountModel() {
        person = new Person();
        role = new Role();
    }

    public AccountModel(String email, String oldPassword, String newPassword, boolean enable, Person person, Role role) {
        this.email = email;
        this.oldPassword = oldPassword;
        this.newPassword = newPassword;
        this.enable = enable;
        this.person = person;
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    

    @Override
    public String toString() {
        return "AccountModel{" + "id=" + id + ", email=" + email + ", oldPassword=" + oldPassword + ", newPassword=" + newPassword + ", enable=" + enable + ", person=" + person.getFirstName() + " " + person.getLastName() + ", role=" + role.getName() + '}';
    }
}
