/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import entity.Account;
import entity.SnackOrderDetail;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author admin
 */
@ManagedBean
@SessionScoped
public class SnackOrderModel implements Serializable{

    private Integer id;
    private String status;
    private Double total;
    private Date orderDate;
    private Account client;
    private List<SnackOrderDetail> snackOrdersDetail;
    
    public SnackOrderModel() {
        total = 0.0;
        snackOrdersDetail = new ArrayList<>();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public Account getClient() {
        return client;
    }

    public void setClient(Account client) {
        this.client = client;
    }

    public List<SnackOrderDetail> getSnackOrdersDetail() {
        return snackOrdersDetail;
    }

    public void setSnackOrdersDetail(List<SnackOrderDetail> snackOrdersDetail) {
        this.snackOrdersDetail = snackOrdersDetail;
    }

    @Override
    public String toString() {
        return "SanckOrderModel{" + "id=" + id + ", status=" + status + ", total=" + total + ", orderDate=" + orderDate + ", client=" + client.getPerson().getFirstName() + ", snackOrdersDetail=" + snackOrdersDetail.size() + '}';
    }
    
    
}
