/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import componet.ValidationExpressionsConstant;
import componet.ValidationMessagesConstant;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.validation.constraints.Pattern;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author admin
 */
@ManagedBean
@RequestScoped
public class RegisterModel implements Serializable{
    
    @NotEmpty()
    @Pattern(regexp = ValidationExpressionsConstant.TEXTO_LIMPIO, message = ValidationMessagesConstant.TEXT_ONLY)
    private String firstName;
    
    @NotEmpty()
    @Pattern(regexp = ValidationExpressionsConstant.TEXTO_LIMPIO, message = ValidationMessagesConstant.TEXT_ONLY)
    private String lastName;
    
    @NotEmpty()
    @Pattern(regexp = ValidationExpressionsConstant.EMAIL, message = ValidationMessagesConstant.VALID_EMAIL)
    private String email;
    
    @NotEmpty()
    private String password;
    
    @NotEmpty()
    private String confirmPassword;
    
    public RegisterModel() {}

    public RegisterModel(String firstName, String lastName, String email, String password, String confirmPassword) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.confirmPassword = confirmPassword;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    @Override
    public String toString() {
        return "RegisterModel{" + "firstName=" + firstName + ", lastName=" + lastName + ", email=" + email + ", password=" + password + ", confirmPassword=" + confirmPassword + '}';
    }
}
