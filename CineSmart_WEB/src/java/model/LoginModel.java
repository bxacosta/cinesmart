/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import componet.ValidationExpressionsConstant;
import componet.ValidationMessagesConstant;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.validation.constraints.Pattern;

/**
 *
 * @author admin
 */
@ManagedBean
@RequestScoped
public class LoginModel implements Serializable{
    
    @Pattern(regexp = ValidationExpressionsConstant.EMAIL, message = ValidationMessagesConstant.VALID_EMAIL)
    private String username;
    
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "LoginModel{" + "username=" + username + ", password=" + password + '}';
    }
}
