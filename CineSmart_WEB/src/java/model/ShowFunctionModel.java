/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import entity.Function;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author admin
 */
@ManagedBean
@RequestScoped
public class ShowFunctionModel implements Serializable{

    private String format;
    private String type;
    private List<Function> functions;
    
    public ShowFunctionModel() {
        functions = new ArrayList<>();
    }

    public ShowFunctionModel(String format, String type, List<Function> functions) {
        this.format = format;
        this.type = type;
        this.functions = functions;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Function> getFunctions() {
        return functions;
    }

    public void setFunctions(List<Function> functions) {
        this.functions = functions;
    }

    @Override
    public String toString() {
        return "ShowFunctionModel{" + "format=" + format + ", type=" + type + ", functions=" + functions.size() + '}';
    }
    
    
}
