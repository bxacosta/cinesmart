/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package componet;

import entity.Account;
import entity.Director;
import entity.Format;
import entity.Function;
import entity.FunctionType;
import entity.Gender;
import entity.Hall;
import entity.Movie;
import entity.Person;
import entity.Role;
import entity.Seat;
import entity.Snack;
import entity.Ticket;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ApplicationScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import service.AccountServiceLocal;
import service.DirectorServiceLocal;
import service.FormatServiceLocal;
import service.FunctionServiceLocal;
import service.FunctionTypeServiceLocal;
import service.GenderServiceLocal;
import service.HallServiceLocal;
import service.MovieServiceLocal;
import service.RoleServiceLocal;
import service.SnackServiceLocal;
import service.TicketServiceLocal;

/**
 *
 * @author admin
 */
@ManagedBean(eager = true)
@ApplicationScoped
public class InitApp {

    private ServletContext sc = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
    
    
    @EJB
    private AccountServiceLocal accountService;
    @EJB
    private RoleServiceLocal roleService;
    @EJB
    private DirectorServiceLocal directorService;
    @EJB
    private GenderServiceLocal genderService;
    @EJB
    private MovieServiceLocal movieService;
    @EJB
    private HallServiceLocal hallService;
    @EJB
    private FormatServiceLocal formaService;
    @EJB
    private FunctionTypeServiceLocal functionTypeService;
    @EJB
    private TicketServiceLocal ticketService;
    @EJB
    private FunctionServiceLocal functionService;
    @EJB
    private SnackServiceLocal snackService;

    @PostConstruct
    public void init() throws ParseException, FileNotFoundException, IOException {
        createRole();
        createDefaultAdminUser();
        createGenders();
        createDirectors();
        createMovies();
        createTheaters();
        createFormat();
        createFunctionType();
        createTicket();
        createFunctions();
        createSnack();
    }

    public void createRole() {
        if (roleService.findAll().isEmpty()) {
            List<Role> roles = new ArrayList<>();
            roles.add(new Role(AppConstant.ADMIN_ROLE, "Administrador"));
            roles.add(new Role(AppConstant.USER_ROLE, "Usuario"));

            for (Role role : roles) {
                roleService.create(role);
            }
        }
    }

    private void createDefaultAdminUser() {
        if (accountService.findAll().isEmpty()) {
            Date date = new Date();
            Role r = roleService.findByRoleName(AppConstant.ADMIN_ROLE);
            Account admin = new Account("admin@mail.com", "admin", true, date, r, new Person("Admin", null));

            accountService.create(admin);
        }
    }

    private void createGenders() {
        if (genderService.findAll().isEmpty()) {
            List<Gender> genders = new ArrayList<>();

            genders.add(new Gender("Acción"));
            genders.add(new Gender("Aventura"));
            genders.add(new Gender("Comedia"));
            genders.add(new Gender("Drama"));
            genders.add(new Gender("Documental"));
            genders.add(new Gender("Horror"));
            genders.add(new Gender("Musical"));
            genders.add(new Gender("Ciencia Ficción"));
            genders.add(new Gender("Belíco"));
            genders.add(new Gender("Romantica"));
            genders.add(new Gender("Detectives"));
            genders.add(new Gender("Biografía"));
            genders.add(new Gender("Animada"));
            genders.add(new Gender("Misterio"));
            genders.add(new Gender("Suspenso"));

            for (Gender gender : genders) {
                genderService.create(gender);
            }
        }
    }

    private void createDirectors() {
        if (directorService.findAll().isEmpty()) {
            List<Director> directors = new ArrayList<>();

            directors.add(new Director("Steven Spielberg"));
            directors.add(new Director("Martin Scorsese"));
            directors.add(new Director("Alfred Hitchcock"));
            directors.add(new Director("Orson Welles"));
            directors.add(new Director("Francis Ford Coppola"));
            directors.add(new Director("Ridley Scott"));
            directors.add(new Director("Akira Kurosawa"));
            directors.add(new Director("Joel Coen"));
            directors.add(new Director("Clint Eastwood"));
            directors.add(new Director("Frank Capra"));
            directors.add(new Director("Christopher Nolan"));
            directors.add(new Director("Roman Polanski"));
            directors.add(new Director("Ingmar Bergman"));
            directors.add(new Director("David Fincher"));
            directors.add(new Director("Peter Jackson"));
            directors.add(new Director("Federico Fellini"));
            directors.add(new Director("Tim Burton"));
            directors.add(new Director("James Cameron"));
            directors.add(new Director("Sidney Lumet"));
            directors.add(new Director("Brian De Palma"));
            directors.add(new Director("Terry Gilliam"));
            directors.add(new Director("Guillermo del Toro"));
            directors.add(new Director("Mel Gibson"));
            directors.add(new Director("Lee Unkrich"));
            directors.add(new Director("Adrian Molina"));
            directors.add(new Director("Roar Uthaug"));
            directors.add(new Director("Steven S. DeKnight"));
            directors.add(new Director("Josh Boone"));
            directors.add(new Director("Martin McDonagh"));
            directors.add(new Director("David Yates"));

            for (Director director : directors) {
                directorService.create(director);
            }

        }
    }

    private void createMovies() throws IOException, ParseException {
        if (movieService.finAll().isEmpty()) {

            List<Movie> movies = new ArrayList<>();
            FileUploadService fs = new FileUploadService();
            // Image path
            String inputPath = sc.getRealPath(File.separator).concat(AppConstant.DEMO_MOVIE_IMAGE_PATH);
            String outputPath = sc.getRealPath(File.separator).concat(AppConstant.MOVIE_IMAGE_PATH);

            String imageLocation = inputPath + "1.jpg";
            byte[] imageData = Files.readAllBytes(Paths.get(imageLocation));
            String imageName = fs.createFileFromBytes(imageData, "jpg", outputPath);
            String synopsis = "Treinta años después de los acontecimientos de la primera película, un nuevo Blade Runner, el oficial K del Departamento de Policía de Los Ángeles (Ryan Gosling), descubre un secreto enterrado durante largo tiempo que tiene el potencial de hundir lo que queda de la sociedad en el caos. El descubrimiento de K le lleva a buscar a Rick Deckard (Harrison Ford), un ex Blade Runner que ha estado desaparecido durante 30 años.";
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            Date date = df.parse("12/11/2017");
            List<Director> directors = new ArrayList<>();
            directors.add(directorService.findByName("Lee Unkrich"));
            directors.add(directorService.findByName("Adrian Molina"));
            List<Gender> genders = new ArrayList<>();
            genders.add(genderService.findByName("Ciencia Ficción"));
            genders.add(genderService.findByName("Aventura"));
            genders.add(genderService.findByName("Suspenso"));
            movies.add(new Movie("Blade Runner 2049", "Blade Runner 2049", "2:45", "Mayores de 12 años.", synopsis, date, "https://youtu.be/gCcx85zbxz4", imageName, imageData, directors, genders));


            imageLocation = inputPath + "2.jpg";
            imageData = Files.readAllBytes(Paths.get(imageLocation));
            imageName = fs.createFileFromBytes(imageData, "jpg", outputPath);
            synopsis = "Un grupo de viajeros, exploradores y soldados de lo más dispar se reúne para viajar a una misteriosa isla del Pacífico que no aparece en los mapas. Entre ellos están el Capitán James Conrad (Tom Hiddleston), el Teniente Coronel Packard (Samuel L. Jackson) y una fotoperiodista (Brie Larson) amante de la naturaleza. Pero al adentrarse en esta bella y también traicionera isla, los exploradores encontrarán algo absolutamente sorprendente.";
            date = df.parse("28/02/2017");
            directors.clear();
            directors.add(directorService.findByName("Martin McDonagh"));
            genders.clear();
            genders.add(genderService.findByName("Aventura"));
            genders.add(genderService.findByName("Suspenso"));
            movies.add(new Movie("Kong - La Isla Calavera", "Kong - Skull Island", "2:15", "Mayores de 12 años", synopsis, date, "https://youtu.be/8wP_3OO3Res", imageName, imageData, directors, genders));


            imageLocation = inputPath + "3.jpg";
            imageData = Files.readAllBytes(Paths.get(imageLocation));
            imageName = fs.createFileFromBytes(imageData, "jpg", outputPath);
            synopsis = "Estados Unidos, alrededor de 1963. Es la Guerra Fría y la carrera militar y espacial está en su punto más álgido. Elisa es una empleada de la limpieza muda en una instalación del Gobierno que esconde unos laboratorios secretos. Su vida cambia por completo al descubrir a un ser enigmático: un hombre-pez único, una auténtica anomalía natural, que vive encerrado y es víctima de diversos experimentos. Elisa empieza entonces a sentir simpatía por este extraño ser y se establece una fuerte conexión entre ambos. Pero el mundo real no es un lugar seguro para un hombre de estas características.";
            date = df.parse("23/02/2018");
            directors.clear();
            directors.add(directorService.findByName("Guillermo del Toro"));
            genders.clear();
            genders.add(genderService.findByName("Ciencia Ficción"));
            genders.add(genderService.findByName("Misterio"));
            genders.add(genderService.findByName("Drama"));
            movies.add(new Movie("Pantera Negra", "Black Panter", "2:30", "Mayores de 12 años", synopsis, date, "https://youtu.be/ITqLYivTuh4", imageName, imageData, directors, genders));
            
            
            imageLocation = inputPath + "4.jpg";
            imageData = Files.readAllBytes(Paths.get(imageLocation));
            imageName = fs.createFileFromBytes(imageData, "jpg", outputPath);
            synopsis = "Thomas ha vuelto con más fuerzas que nunca. Su objetivo es encontrar de una vez por todas la cura para la Llamarada, la enfermedad que ha asolado el mundo. Además, él y sus compañeros tendrán que resolver todas las preguntas que les persiguen desde que un día aparecieran en el Claro del Laberinto. Para ello, tendrán que volver al punto de partida donde empezaron, pero el camino no será nada fácil. ¿Conseguirán dar respuesta a todos los interrogantes sobre el laberinto y los que lo crearon.";
            date = df.parse("09/02/2018");
            directors.clear();
            directors.add(directorService.findByName("Guillermo del Toro"));
            genders.clear();
            genders.add(genderService.findByName("Ciencia Ficción"));
            genders.add(genderService.findByName("Misterio"));
            genders.add(genderService.findByName("Drama"));
            movies.add(new Movie("Maze Runner - La Cura Mortal", "Maze Runner - Death Cure", "2:00", "Mayores de 12 años", synopsis, date, "https://youtu.be/ITqLYivTuh4", imageName, imageData, directors, genders));
            
            
            imageLocation = inputPath + "5.jpg";
            imageData = Files.readAllBytes(Paths.get(imageLocation));
            imageName = fs.createFileFromBytes(imageData, "jpg", outputPath);
            synopsis = "Estados Unidos, alrededor de 1963. Es la Guerra Fría y la carrera militar y espacial está en su punto más álgido. Elisa es una empleada de la limpieza muda en una instalación del Gobierno que esconde unos laboratorios secretos. Su vida cambia por completo al descubrir a un ser enigmático: un hombre-pez único, una auténtica anomalía natural, que vive encerrado y es víctima de diversos experimentos. Elisa empieza entonces a sentir simpatía por este extraño ser y se establece una fuerte conexión entre ambos. Pero el mundo real no es un lugar seguro para un hombre de estas características.";
            date = df.parse("23/02/2018");
            directors.clear();
            directors.add(directorService.findByName("Guillermo del Toro"));
            genders.clear();
            genders.add(genderService.findByName("Misterio"));
            genders.add(genderService.findByName("Drama"));
            movies.add(new Movie("La Forma del Agua", "The Shape of Water", "2:00", "Mayores de 12 años", synopsis, date, "https://youtu.be/ITqLYivTuh4", imageName, imageData, directors, genders));

            
            imageLocation = inputPath + "6.jpg";
            imageData = Files.readAllBytes(Paths.get(imageLocation));
            imageName = fs.createFileFromBytes(imageData, "jpg", outputPath);
            synopsis = "Esta vez, el pirata más excéntrico del cine, el Capitán Jack Sparrow (Johnny Depp), no podrá escapar de su destino tan fácilmente. En su camino se cruzará un peligroso villano, Salazar (Javier Bardem), un Capitán que junto a su tripulación acaban de escapar del temible Triángulo del Diablo.";
            date = df.parse("23/02/2018");
            directors.clear();
            directors.add(directorService.findByName("Guillermo del Toro"));
            genders.clear();
            genders.add(genderService.findByName("Misterio"));
            genders.add(genderService.findByName("Drama"));
            movies.add(new Movie("Piratas del Caribe - La Venganza de Salazar", "Pirates Of The Cariben - Death Men Tell no Tales", "2:00", "Mayores de 12 años", synopsis, date, "https://youtu.be/azjsS0wxTA8", imageName, imageData, directors, genders));
            
            
            imageLocation = inputPath + "7.jpg";
            imageData = Files.readAllBytes(Paths.get(imageLocation));
            imageName = fs.createFileFromBytes(imageData, "jpg", outputPath);
            synopsis = "Estados Unidos, alrededor de 1963. Es la Guerra Fría y la carrera militar y espacial está en su punto más álgido. Elisa es una empleada de la limpieza muda en una instalación del Gobierno que esconde unos laboratorios secretos. Su vida cambia por completo al descubrir a un ser enigmático: un hombre-pez único, una auténtica anomalía natural, que vive encerrado y es víctima de diversos experimentos. Elisa empieza entonces a sentir simpatía por este extraño ser y se establece una fuerte conexión entre ambos. Pero el mundo real no es un lugar seguro para un hombre de estas características.";
            date = df.parse("23/02/2018");
            directors.clear();
            directors.add(directorService.findByName("Guillermo del Toro"));
            genders.clear();
            genders.add(genderService.findByName("Misterio"));
            genders.add(genderService.findByName("Drama"));
            movies.add(new Movie("Ghost in the Shell - Vigilante del Futuro", "Ghost in the Shell", "2:50", "Mayores de 12 años", synopsis, date, "https://youtu.be/tRkb1X9ovI4", imageName, imageData, directors, genders));
            
            
            imageLocation = inputPath + "8.jpg";
            imageData = Files.readAllBytes(Paths.get(imageLocation));
            imageName = fs.createFileFromBytes(imageData, "jpg", outputPath);
            synopsis = "Motivado por la fe que había recuperado en la humanidad e inspirado por la acción altruista de Superman, Bruce Wayne recluta la ayuda de su nueva aliada, Diana Prince, para enfrentarse a un enemigo aún mayor. Juntos, Batman y Wonder Woman se mueven rápidamente para intentar encontrar y reclutar un equipo de metahumanos que combata esta nueva amenaza.";
            date = df.parse("23/02/2018");
            directors.clear();
            directors.add(directorService.findByName("Guillermo del Toro"));
            genders.clear();
            genders.add(genderService.findByName("Misterio"));
            genders.add(genderService.findByName("Drama"));
            movies.add(new Movie("La Liga de la Justicia", "Liga de la Justicia", "2:50", "Mayores de 12 años", synopsis, date, "https://youtu.be/3cxixDgHUYw", imageName, imageData, directors, genders));
            
            for (Movie movie : movies) {
                movieService.create(movie);
            }
        }
    }

    private void createTheaters() throws IOException {
        if (hallService.findAll().isEmpty()) {
            List<Hall> theaters = new ArrayList<>();
            FileUploadService fs = new FileUploadService();
            
            String inputPath = sc.getRealPath(File.separator).concat(AppConstant.DEMO_HALL_IMAGE_PATH);
            String outputPath = sc.getRealPath(File.separator).concat(AppConstant.HALL_IMAGE_PATH);

            byte[] imageData = Files.readAllBytes(Paths.get(inputPath + "sala1.jpg"));
            String imageName = fs.createFileFromBytes(imageData, "jpg", outputPath);
            List<Seat> seats = new ArrayList<>();
            String[] rows = new String[]{"A", "B", "C", "D", "F", "G", "H", "I", "H", "J"};
            for (String row : rows) {
                for (Integer i = 1; i <= 10; i++) {
                    seats.add(new Seat(row, i.toString(), "Simple"));
                }
            }
            theaters.add(new Hall("Sala 1", imageName, imageData, seats));
             
            
            imageData = Files.readAllBytes(Paths.get(inputPath + "sala2.jpg"));
            imageName = fs.createFileFromBytes(imageData, "jpg", outputPath);
            seats = new ArrayList<>();
            rows = new String[]{"A", "B", "C", "D", "F", "G", "H", "I", "H", "J"};
            for (String row : rows) {
                for (Integer i = 1; i <= 10; i++) {
                    seats.add(new Seat(row, i.toString(), "Simple"));
                }
            }
            theaters.add(new Hall("Sala 2", imageName, imageData, seats));
            
            
            imageData = Files.readAllBytes(Paths.get(inputPath + "sala3.jpg"));
            imageName = fs.createFileFromBytes(imageData, "jpg", outputPath);
            seats = new ArrayList<>();
            rows = new String[]{"A", "B", "C", "D", "F", "G", "H", "I", "H", "J"};
            for (String row : rows) {
                for (Integer i = 1; i <= 10; i++) {
                    seats.add(new Seat(row, i.toString(), "Simple"));
                }
            }
            theaters.add(new Hall("Sala 3", imageName, imageData, seats));

            
            imageData = Files.readAllBytes(Paths.get(inputPath + "sala4.jpg"));
            imageName = fs.createFileFromBytes(imageData, "jpg", outputPath);
            seats = new ArrayList<>();
            rows = new String[]{"A", "B", "C", "D", "F", "G", "H", "I", "H", "J", "K", "L"};
            for (String row : rows) {
                for (Integer i = 1; i <= 8; i++) {
                    seats.add(new Seat(row, i.toString(), "Simple"));
                }
            }
            theaters.add(new Hall("Sala 4", imageName, imageData, seats));
            
            
            imageData = Files.readAllBytes(Paths.get(inputPath + "sala5.jpg"));
            imageName = fs.createFileFromBytes(imageData, "jpg", outputPath);
            seats = new ArrayList<>();
            rows = new String[]{"A", "B", "C", "D", "F", "G", "H", "I", "H", "J", "K", "L"};
            for (String row : rows) {
                for (Integer i = 1; i <= 8; i++) {
                    seats.add(new Seat(row, i.toString(), "Simple"));
                }
            }
            theaters.add(new Hall("Sala 5", imageName, imageData, seats));
            
            
            imageData = Files.readAllBytes(Paths.get(inputPath + "sala6.jpg"));
            imageName = fs.createFileFromBytes(imageData, "jpg", outputPath);
            seats = new ArrayList<>();
            rows = new String[]{"A", "B", "C", "D", "F", "G", "H", "I", "H", "J", "K", "L"};
            for (String row : rows) {
                for (Integer i = 1; i <= 8; i++) {
                    seats.add(new Seat(row, i.toString(), "Simple"));
                }
            }
            theaters.add(new Hall("Sala 6", imageName, imageData, seats));
           
            
            imageData = Files.readAllBytes(Paths.get(inputPath + "sala7.jpg"));
            imageName = fs.createFileFromBytes(imageData, "jpg", outputPath);
            seats = new ArrayList<>();
            rows = new String[]{"A", "B", "C", "D", "F", "G", "H", "I", "H", "J", "K", "L"};
            for (String row : rows) {
                for (Integer i = 1; i <= 8; i++) {
                    seats.add(new Seat(row, i.toString(), "Simple"));
                }
            }
            theaters.add(new Hall("Sala 7", imageName, imageData, seats));

            for (Hall hall : theaters) {
                hallService.create(hall);
            }
        }
    }

    private void createFormat() {
        if (formaService.findAll().isEmpty()) {
            List<Format> formats = new ArrayList<>();

            formats.add(new Format("2D"));
            formats.add(new Format("3D"));
            formats.add(new Format("2D IMAX"));
            formats.add(new Format("3D IMAX"));

            for (Format format : formats) {
                formaService.create(format);
            }

        }
    }

    private void createFunctionType() {
        if (functionTypeService.findAll().isEmpty()) {
            List<FunctionType> functionTypes = new ArrayList<>();

            functionTypes.add(new FunctionType("Español"));
            functionTypes.add(new FunctionType("Subtitulada"));

            for (FunctionType functionType : functionTypes) {
                functionTypeService.create(functionType);
            }
        }
    }

    private void createTicket() {
        if (ticketService.findAll().isEmpty()) {
            List<Ticket> tickets = new ArrayList<>();

            tickets.add(new Ticket("General Adulto", 4.60));
            tickets.add(new Ticket("Adulto Fin de Semana", 6.00));
            tickets.add(new Ticket("3D General Adulto", 5.60));
            tickets.add(new Ticket("3D Adulto Fin de Semana", 7.00));

            tickets.add(new Ticket("General Niño", 3.60));
            tickets.add(new Ticket("Niño Fin de Semana", 5.00));
            tickets.add(new Ticket("3D General Niño", 4.60));
            tickets.add(new Ticket("3D Niño Fin de Semana", 6.00));

            tickets.add(new Ticket("Persona con Discapacidad", 3.20));
            tickets.add(new Ticket("3D Persona con Discapacidad", 3.60));

            tickets.add(new Ticket("Tercera Edad", 3.20));
            tickets.add(new Ticket("3D Tercera Edad", 3.60));

            for (Ticket ticket : tickets) {
                ticketService.create(ticket);
            }
        }
    }

    private void createFunctions() {
        if (functionService.findAll().isEmpty()) {
            List<Function> functions = new ArrayList<>();
            Calendar c = Calendar.getInstance();
            List<Ticket> tikets = new ArrayList<>();
            Date functionDate;
            
            
            //Blade Runner 2049 - 3D Español
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 13);
            c.set(Calendar.MINUTE, 15);
            functionDate = c.getTime();
            tikets.add(ticketService.findByName("3D General Adulto"));
            tikets.add(ticketService.findByName("3D General Niño"));
            tikets.add(ticketService.findByName("3D Persona con Discapacidad"));
            tikets.add(ticketService.findByName("3D Tercera Edad"));
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 2")
                    , movieService.findByTitle("Blade Runner 2049")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("3D")
                    , tikets));
  
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 15);
            c.set(Calendar.MINUTE, 30);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 1")
                    , movieService.findByTitle("Blade Runner 2049")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("3D")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 17);
            c.set(Calendar.MINUTE, 45);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 3")
                    , movieService.findByTitle("Blade Runner 2049")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("3D")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 20);
            c.set(Calendar.MINUTE, 15);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 3")
                    , movieService.findByTitle("Blade Runner 2049")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("3D")
                    , tikets));
            
            // 2D Español - Blade Runner 2049
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 15);
            c.set(Calendar.MINUTE, 10);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 4")
                    , movieService.findByTitle("Blade Runner 2049")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("2D")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 18);
            c.set(Calendar.MINUTE, 15);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 4")
                    , movieService.findByTitle("Blade Runner 2049")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("2D")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 21);
            c.set(Calendar.MINUTE, 45);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 4")
                    , movieService.findByTitle("Blade Runner 2049")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("2D")
                    , tikets));
            
            // 2D Subtitulada - Blade Runner 2049
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 14);
            c.set(Calendar.MINUTE, 25);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 4")
                    , movieService.findByTitle("Blade Runner 2049")
                    , functionTypeService.findByName("Subtitulada")
                    , formaService.findByName("2D")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 18);
            c.set(Calendar.MINUTE, 15);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 3")
                    , movieService.findByTitle("Blade Runner 2049")
                    , functionTypeService.findByName("Subtitulada")
                    , formaService.findByName("2D")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 21);
            c.set(Calendar.MINUTE, 45);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 1")
                    , movieService.findByTitle("Blade Runner 2049")
                    , functionTypeService.findByName("Subtitulada")
                    , formaService.findByName("2D")
                    , tikets));
            
            // 3D IMAX Español - Blade Runner 2049
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 13);
            c.set(Calendar.MINUTE, 10);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 1")
                    , movieService.findByTitle("Blade Runner 2049")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("3D IMAX")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 15);
            c.set(Calendar.MINUTE, 50);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 2")
                    , movieService.findByTitle("Blade Runner 2049")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("3D IMAX")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 19);
            c.set(Calendar.MINUTE, 40);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 6")
                    , movieService.findByTitle("Blade Runner 2049")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("3D IMAX")
                    , tikets));
            
            /************************************************************/
            /*                   Kong - La Isla Calavera                */
            /************************************************************/
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 15);
            c.set(Calendar.MINUTE, 30);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 1")
                    , movieService.findByTitle("Kong - La Isla Calavera")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("3D")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 17);
            c.set(Calendar.MINUTE, 45);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 3")
                    , movieService.findByTitle("Kong - La Isla Calavera")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("3D")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 20);
            c.set(Calendar.MINUTE, 15);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 3")
                    , movieService.findByTitle("Kong - La Isla Calavera")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("3D")
                    , tikets));
            
            // 2D Español - Blade Runner 2049
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 15);
            c.set(Calendar.MINUTE, 10);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 4")
                    , movieService.findByTitle("Kong - La Isla Calavera")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("2D")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 18);
            c.set(Calendar.MINUTE, 15);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 4")
                    , movieService.findByTitle("Kong - La Isla Calavera")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("2D")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 21);
            c.set(Calendar.MINUTE, 45);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 4")
                    , movieService.findByTitle("Kong - La Isla Calavera")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("2D")
                    , tikets));
            
            // 2D Subtitulada - Blade Runner 2049
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 14);
            c.set(Calendar.MINUTE, 25);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 4")
                    , movieService.findByTitle("Kong - La Isla Calavera")
                    , functionTypeService.findByName("Subtitulada")
                    , formaService.findByName("2D")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 18);
            c.set(Calendar.MINUTE, 15);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 3")
                    , movieService.findByTitle("Kong - La Isla Calavera")
                    , functionTypeService.findByName("Subtitulada")
                    , formaService.findByName("2D")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 21);
            c.set(Calendar.MINUTE, 45);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 1")
                    , movieService.findByTitle("Kong - La Isla Calavera")
                    , functionTypeService.findByName("Subtitulada")
                    , formaService.findByName("2D")
                    , tikets));
            
            // 3D IMAX Español - Blade Runner 2049
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 13);
            c.set(Calendar.MINUTE, 10);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 1")
                    , movieService.findByTitle("Kong - La Isla Calavera")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("3D IMAX")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 15);
            c.set(Calendar.MINUTE, 50);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 2")
                    , movieService.findByTitle("Kong - La Isla Calavera")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("3D IMAX")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 19);
            c.set(Calendar.MINUTE, 40);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 6")
                    , movieService.findByTitle("Kong - La Isla Calavera")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("3D IMAX")
                    , tikets));
            
            
            /************************************************************/
            /*                   Pantera Negra                          */
            /************************************************************/
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 15);
            c.set(Calendar.MINUTE, 30);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 1")
                    , movieService.findByTitle("Pantera Negra")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("3D")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 17);
            c.set(Calendar.MINUTE, 45);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 3")
                    , movieService.findByTitle("Pantera Negra")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("3D")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 20);
            c.set(Calendar.MINUTE, 15);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 3")
                    , movieService.findByTitle("Pantera Negra")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("3D")
                    , tikets));
            
            // 2D Español - Blade Runner 2049
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 15);
            c.set(Calendar.MINUTE, 10);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 4")
                    , movieService.findByTitle("Pantera Negra")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("2D")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 18);
            c.set(Calendar.MINUTE, 15);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 4")
                    , movieService.findByTitle("Pantera Negra")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("2D")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 21);
            c.set(Calendar.MINUTE, 45);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 4")
                    , movieService.findByTitle("Pantera Negra")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("2D")
                    , tikets));
            
            // 2D Subtitulada - Blade Runner 2049
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 14);
            c.set(Calendar.MINUTE, 25);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 4")
                    , movieService.findByTitle("Pantera Negra")
                    , functionTypeService.findByName("Subtitulada")
                    , formaService.findByName("2D")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 18);
            c.set(Calendar.MINUTE, 15);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 3")
                    , movieService.findByTitle("Pantera Negra")
                    , functionTypeService.findByName("Subtitulada")
                    , formaService.findByName("2D")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 21);
            c.set(Calendar.MINUTE, 45);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 1")
                    , movieService.findByTitle("Pantera Negra")
                    , functionTypeService.findByName("Subtitulada")
                    , formaService.findByName("2D")
                    , tikets));
            
            // 3D IMAX Español - Blade Runner 2049
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 13);
            c.set(Calendar.MINUTE, 10);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 1")
                    , movieService.findByTitle("Pantera Negra")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("3D IMAX")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 15);
            c.set(Calendar.MINUTE, 50);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 2")
                    , movieService.findByTitle("Pantera Negra")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("3D IMAX")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 19);
            c.set(Calendar.MINUTE, 40);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 6")
                    , movieService.findByTitle("Pantera Negra")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("3D IMAX")
                    , tikets));
            
            
            /************************************************************/
            /*                   Pantera Negra                          */
            /************************************************************/
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 15);
            c.set(Calendar.MINUTE, 30);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 1")
                    , movieService.findByTitle("Pantera Negra")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("3D")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 17);
            c.set(Calendar.MINUTE, 45);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 3")
                    , movieService.findByTitle("Pantera Negra")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("3D")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 20);
            c.set(Calendar.MINUTE, 15);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 3")
                    , movieService.findByTitle("Pantera Negra")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("3D")
                    , tikets));
            
            // 2D Español - Blade Runner 2049
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 15);
            c.set(Calendar.MINUTE, 10);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 4")
                    , movieService.findByTitle("Pantera Negra")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("2D")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 18);
            c.set(Calendar.MINUTE, 15);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 4")
                    , movieService.findByTitle("Pantera Negra")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("2D")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 21);
            c.set(Calendar.MINUTE, 45);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 4")
                    , movieService.findByTitle("Pantera Negra")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("2D")
                    , tikets));
            
            // 2D Subtitulada - Blade Runner 2049
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 14);
            c.set(Calendar.MINUTE, 25);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 4")
                    , movieService.findByTitle("Pantera Negra")
                    , functionTypeService.findByName("Subtitulada")
                    , formaService.findByName("2D")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 18);
            c.set(Calendar.MINUTE, 15);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 3")
                    , movieService.findByTitle("Pantera Negra")
                    , functionTypeService.findByName("Subtitulada")
                    , formaService.findByName("2D")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 21);
            c.set(Calendar.MINUTE, 45);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 1")
                    , movieService.findByTitle("Pantera Negra")
                    , functionTypeService.findByName("Subtitulada")
                    , formaService.findByName("2D")
                    , tikets));
            
            // 3D IMAX Español - Blade Runner 2049
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 13);
            c.set(Calendar.MINUTE, 10);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 1")
                    , movieService.findByTitle("Pantera Negra")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("3D IMAX")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 15);
            c.set(Calendar.MINUTE, 50);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 2")
                    , movieService.findByTitle("Pantera Negra")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("3D IMAX")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 19);
            c.set(Calendar.MINUTE, 40);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 6")
                    , movieService.findByTitle("Pantera Negra")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("3D IMAX")
                    , tikets));
            
            
            /************************************************************/
            /*             Maze Runner - La Cura Mortal                 */
            /************************************************************/
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 15);
            c.set(Calendar.MINUTE, 30);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 1")
                    , movieService.findByTitle("Maze Runner - La Cura Mortal")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("3D")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 17);
            c.set(Calendar.MINUTE, 45);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 3")
                    , movieService.findByTitle("Maze Runner - La Cura Mortal")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("3D")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 20);
            c.set(Calendar.MINUTE, 15);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 3")
                    , movieService.findByTitle("Maze Runner - La Cura Mortal")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("3D")
                    , tikets));
            
            // 2D Español - Blade Runner 2049
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 15);
            c.set(Calendar.MINUTE, 10);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 4")
                    , movieService.findByTitle("Maze Runner - La Cura Mortal")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("2D")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 18);
            c.set(Calendar.MINUTE, 15);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 4")
                    , movieService.findByTitle("Maze Runner - La Cura Mortal")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("2D")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 21);
            c.set(Calendar.MINUTE, 45);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 4")
                    , movieService.findByTitle("Maze Runner - La Cura Mortal")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("2D")
                    , tikets));
            
            // 2D Subtitulada - Blade Runner 2049
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 14);
            c.set(Calendar.MINUTE, 25);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 4")
                    , movieService.findByTitle("Maze Runner - La Cura Mortal")
                    , functionTypeService.findByName("Subtitulada")
                    , formaService.findByName("2D")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 18);
            c.set(Calendar.MINUTE, 15);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 3")
                    , movieService.findByTitle("Maze Runner - La Cura Mortal")
                    , functionTypeService.findByName("Subtitulada")
                    , formaService.findByName("2D")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 21);
            c.set(Calendar.MINUTE, 45);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 1")
                    , movieService.findByTitle("Maze Runner - La Cura Mortal")
                    , functionTypeService.findByName("Subtitulada")
                    , formaService.findByName("2D")
                    , tikets));
            
            // 3D IMAX Español - Blade Runner 2049
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 13);
            c.set(Calendar.MINUTE, 10);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 1")
                    , movieService.findByTitle("Maze Runner - La Cura Mortal")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("3D IMAX")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 15);
            c.set(Calendar.MINUTE, 50);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 2")
                    , movieService.findByTitle("Maze Runner - La Cura Mortal")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("3D IMAX")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 19);
            c.set(Calendar.MINUTE, 40);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 6")
                    , movieService.findByTitle("Maze Runner - La Cura Mortal")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("3D IMAX")
                    , tikets));
            
            
            /************************************************************/
            /*                      La Forma del Agua                   */
            /************************************************************/
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 15);
            c.set(Calendar.MINUTE, 30);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 1")
                    , movieService.findByTitle("La Forma del Agua")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("3D")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 17);
            c.set(Calendar.MINUTE, 45);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 3")
                    , movieService.findByTitle("La Forma del Agua")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("3D")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 20);
            c.set(Calendar.MINUTE, 15);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 3")
                    , movieService.findByTitle("La Forma del Agua")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("3D")
                    , tikets));
            
            // 2D Español - Blade Runner 2049
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 15);
            c.set(Calendar.MINUTE, 10);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 4")
                    , movieService.findByTitle("La Forma del Agua")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("2D")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 18);
            c.set(Calendar.MINUTE, 15);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 4")
                    , movieService.findByTitle("La Forma del Agua")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("2D")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 21);
            c.set(Calendar.MINUTE, 45);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 4")
                    , movieService.findByTitle("La Forma del Agua")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("2D")
                    , tikets));
            
            // 2D Subtitulada - Blade Runner 2049
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 14);
            c.set(Calendar.MINUTE, 25);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 4")
                    , movieService.findByTitle("La Forma del Agua")
                    , functionTypeService.findByName("Subtitulada")
                    , formaService.findByName("2D")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 18);
            c.set(Calendar.MINUTE, 15);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 3")
                    , movieService.findByTitle("La Forma del Agua")
                    , functionTypeService.findByName("Subtitulada")
                    , formaService.findByName("2D")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 21);
            c.set(Calendar.MINUTE, 45);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 1")
                    , movieService.findByTitle("La Forma del Agua")
                    , functionTypeService.findByName("Subtitulada")
                    , formaService.findByName("2D")
                    , tikets));
            
            // 3D IMAX Español - Blade Runner 2049
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 13);
            c.set(Calendar.MINUTE, 10);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 1")
                    , movieService.findByTitle("La Forma del Agua")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("3D IMAX")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 15);
            c.set(Calendar.MINUTE, 50);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 2")
                    , movieService.findByTitle("La Forma del Agua")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("3D IMAX")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 19);
            c.set(Calendar.MINUTE, 40);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 6")
                    , movieService.findByTitle("La Forma del Agua")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("3D IMAX")
                    , tikets));
            
            /************************************************************/
            /*         Ghost in the Shell - Vigilante del Futuro        */
            /************************************************************/
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 15);
            c.set(Calendar.MINUTE, 30);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 1")
                    , movieService.findByTitle("Ghost in the Shell - Vigilante del Futuro")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("3D")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 17);
            c.set(Calendar.MINUTE, 45);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 3")
                    , movieService.findByTitle("Ghost in the Shell - Vigilante del Futuro")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("3D")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 20);
            c.set(Calendar.MINUTE, 15);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 3")
                    , movieService.findByTitle("Ghost in the Shell - Vigilante del Futuro")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("3D")
                    , tikets));
            
            // 2D Español - Blade Runner 2049
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 15);
            c.set(Calendar.MINUTE, 10);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 4")
                    , movieService.findByTitle("Ghost in the Shell - Vigilante del Futuro")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("2D")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 18);
            c.set(Calendar.MINUTE, 15);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 4")
                    , movieService.findByTitle("Ghost in the Shell - Vigilante del Futuro")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("2D")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 21);
            c.set(Calendar.MINUTE, 45);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 4")
                    , movieService.findByTitle("Ghost in the Shell - Vigilante del Futuro")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("2D")
                    , tikets));
            
            // 2D Subtitulada - Blade Runner 2049
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 14);
            c.set(Calendar.MINUTE, 25);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 4")
                    , movieService.findByTitle("Ghost in the Shell - Vigilante del Futuro")
                    , functionTypeService.findByName("Subtitulada")
                    , formaService.findByName("2D")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 18);
            c.set(Calendar.MINUTE, 15);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 3")
                    , movieService.findByTitle("Ghost in the Shell - Vigilante del Futuro")
                    , functionTypeService.findByName("Subtitulada")
                    , formaService.findByName("2D")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 21);
            c.set(Calendar.MINUTE, 45);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 1")
                    , movieService.findByTitle("Ghost in the Shell - Vigilante del Futuro")
                    , functionTypeService.findByName("Subtitulada")
                    , formaService.findByName("2D")
                    , tikets));
            
            // 3D IMAX Español - Blade Runner 2049
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 13);
            c.set(Calendar.MINUTE, 10);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 1")
                    , movieService.findByTitle("Ghost in the Shell - Vigilante del Futuro")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("3D IMAX")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 15);
            c.set(Calendar.MINUTE, 50);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 2")
                    , movieService.findByTitle("Ghost in the Shell - Vigilante del Futuro")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("3D IMAX")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 19);
            c.set(Calendar.MINUTE, 40);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 6")
                    , movieService.findByTitle("Ghost in the Shell - Vigilante del Futuro")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("3D IMAX")
                    , tikets));
            
            /************************************************************/
            /*                    La Liga de la Justicia                */
            /************************************************************/
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 15);
            c.set(Calendar.MINUTE, 30);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 1")
                    , movieService.findByTitle("La Liga de la Justicia")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("3D")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 17);
            c.set(Calendar.MINUTE, 45);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 3")
                    , movieService.findByTitle("La Liga de la Justicia")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("3D")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 20);
            c.set(Calendar.MINUTE, 15);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 3")
                    , movieService.findByTitle("La Liga de la Justicia")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("3D")
                    , tikets));
            
            // 2D Español - Blade Runner 2049
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 15);
            c.set(Calendar.MINUTE, 10);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 4")
                    , movieService.findByTitle("La Liga de la Justicia")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("2D")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 18);
            c.set(Calendar.MINUTE, 15);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 4")
                    , movieService.findByTitle("La Liga de la Justicia")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("2D")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 21);
            c.set(Calendar.MINUTE, 45);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 4")
                    , movieService.findByTitle("La Liga de la Justicia")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("2D")
                    , tikets));
            
            // 2D Subtitulada - Blade Runner 2049
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 14);
            c.set(Calendar.MINUTE, 25);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 4")
                    , movieService.findByTitle("La Liga de la Justicia")
                    , functionTypeService.findByName("Subtitulada")
                    , formaService.findByName("2D")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 18);
            c.set(Calendar.MINUTE, 15);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 3")
                    , movieService.findByTitle("La Liga de la Justicia")
                    , functionTypeService.findByName("Subtitulada")
                    , formaService.findByName("2D")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 21);
            c.set(Calendar.MINUTE, 45);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 1")
                    , movieService.findByTitle("La Liga de la Justicia")
                    , functionTypeService.findByName("Subtitulada")
                    , formaService.findByName("2D")
                    , tikets));
            
            // 3D IMAX Español - Blade Runner 2049
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 13);
            c.set(Calendar.MINUTE, 10);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 1")
                    , movieService.findByTitle("La Liga de la Justicia")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("3D IMAX")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 15);
            c.set(Calendar.MINUTE, 50);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 2")
                    , movieService.findByTitle("La Liga de la Justicia")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("3D IMAX")
                    , tikets));
            
            c.setTime(new Date());
            c.set(Calendar.HOUR_OF_DAY, 19);
            c.set(Calendar.MINUTE, 40);
            functionDate = c.getTime();
            functions.add(new Function(functionDate
                    , hallService.findByName("Sala 6")
                    , movieService.findByTitle("La Liga de la Justicia")
                    , functionTypeService.findByName("Español")
                    , formaService.findByName("3D IMAX")
                    , tikets));
                    
            
            for (Function fucntion : functions) {
                functionService.create(fucntion);
            }
        }
    }
    
    private void createSnack() throws IOException {
        
        if (snackService.findAll().isEmpty()) {
            List<Snack> snacks = new ArrayList<>();
            
            ServletContext sc = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
            FileUploadService fs = new FileUploadService();
            // Image path
            String inputPath = sc.getRealPath(File.separator).concat(AppConstant.DEMO_SNACK_IMAGE_PATH);
            String outputPath = sc.getRealPath(File.separator).concat(AppConstant.SNACK_IMAGE_PATH);

            System.out.println("OUTPUT PATH: " + inputPath);
            System.out.println("INPUT PATH: " + outputPath);

            String imageLocation = inputPath + "combo1.jpg";
            byte[] imageData = Files.readAllBytes(Paths.get(imageLocation));
            String imageName = fs.createFileFromBytes(imageData, "jpg", outputPath);
            String descriptio = "1 Canguil mediano, 1 Bebida mediana, 1 Hot Dog.";
            snacks.add(new Snack("Combo 1", 7.60, descriptio, imageName, imageData));
            
            imageLocation = inputPath + "combo2.jpg";
            imageData = Files.readAllBytes(Paths.get(imageLocation));
            imageName = fs.createFileFromBytes(imageData, "jpg", outputPath);
            descriptio = "1 Canguil mediano, 1 Bebida mediana, 1 nacho con Cheese o Chili.";
            snacks.add(new Snack("Combo 2", 7.60, descriptio, imageName, imageData));
            
            imageLocation = inputPath + "combo3.jpg";
            imageData = Files.readAllBytes(Paths.get(imageLocation));
            imageName = fs.createFileFromBytes(imageData, "jpg", outputPath);
            descriptio = "1 Canguil mediano, 2 Bebidas medianas, 2 nachos o 2 Hot Dog o 1 nacho y 1 Hot Dog.";
            snacks.add(new Snack("Combo 3", 12.90, descriptio, imageName, imageData));
            
            imageLocation = inputPath + "combo4.jpg";
            imageData = Files.readAllBytes(Paths.get(imageLocation));
            imageName = fs.createFileFromBytes(imageData, "jpg", outputPath);
            descriptio = "1 Canguil mediano, 2 Bebidas medianas.";
            snacks.add(new Snack("Combo 4", 7.60, descriptio, imageName, imageData));
            
            imageLocation = inputPath + "combo5.jpg";
            imageData = Files.readAllBytes(Paths.get(imageLocation));
            imageName = fs.createFileFromBytes(imageData, "jpg", outputPath);
            descriptio = "1 Canguil mediano, 1 Bebida mediana, 1 Mega nacho con Cheese o Chili.";
            snacks.add(new Snack("Combo 5", 9.15, descriptio, imageName, imageData));

            imageLocation = inputPath + "combo_infantil.jpg";
            imageData = Files.readAllBytes(Paths.get(imageLocation));
            imageName = fs.createFileFromBytes(imageData, "jpg", outputPath);
            descriptio = "1 Canguil infantil, 1 Bebida infantil, 1 Dulce.";
            snacks.add(new Snack("Combo Infantil", 5.10, descriptio, imageName, imageData));
            
            
            for (Snack snack : snacks) {
                snackService.create(snack);
            }
        }
    }
}
