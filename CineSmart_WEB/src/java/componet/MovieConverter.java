/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package componet;

import entity.Movie;
import java.util.Date;
import model.MovieModel;

/**
 *
 * @author admin
 */
public class MovieConverter {
    
    public Movie convertMovieModelToMovie(MovieModel movieModel) {
        Movie m = new Movie();
        
        m.setTitle(movieModel.getTitle());
        m.setOriginalTitle(movieModel.getOriginalTitle());
        m.setRuntime(movieModel.getRuntime());
        m.setCensure(movieModel.getCensure());
        m.setSynopsis(movieModel.getSynopsis());
        m.setReleaseDate(movieModel.getReleaseDate());
        m.setTrailer(movieModel.getTrailer());
        m.setImageName(movieModel.getImageName());
        m.setImageData(movieModel.getImageData());
        m.setGenders(movieModel.getGenders());
        m.setDirectors(movieModel.getDirectors());
        
        return m;
    }
}
