/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package componet;

import entity.Function;
import java.util.Date;
import model.FunctionModel;

/**
 *
 * @author admin
 */
public class FunctionConverter {
    
    public Function convertFunctionModelToFunction(FunctionModel functionModel, Date date) {
        Function f = new Function();
        
        f.setMovie(functionModel.getMovie());
        f.setFormat(functionModel.getFormat());
        f.setHall(functionModel.getHall());
        f.setType(functionModel.getType());
        f.setTickets(functionModel.getTickets());
        f.setFunctionDateTime(date);

        return f;
    }
}
