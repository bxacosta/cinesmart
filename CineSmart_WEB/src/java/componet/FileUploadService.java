/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package componet;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author admin
 */
public class FileUploadService {

    public String createFileFromUploadedFile(UploadedFile file, String extension, String path) throws IOException {

        // Define un nombre aleatorio para el archivo (solo funciona con imagenes)
        String fileExtension = file.getContentType().split("/")[1];
        String fileName = randomString(32) + "." + fileExtension;

        // Define los streams de entrada y salida
        InputStream in = file.getInputstream();
        FileOutputStream out = new FileOutputStream(new File(path + fileName));

        // Crea el archivo
        int bytesRead;
        while ((bytesRead = in.read()) != -1) {
            out.write(bytesRead);
        }

        out.flush();
        out.close();
        in.close();

        return fileName;
    }
    
    public String createFileFromBytes(byte[] bytes, String extension, String path) throws IOException {

        // Define un nombre aleatorio para el archivo (solo funciona con imagenes)
        String fileName = randomString(32) + "." + extension;

        // Define los streams de entrada y salida
        InputStream in = new ByteArrayInputStream(bytes);
        FileOutputStream out = new FileOutputStream(new File(path + fileName));

        // Crea el archivo
        int bytesRead;
        while ((bytesRead = in.read()) != -1) {
            out.write(bytesRead);
        }

        out.flush();
        out.close();
        in.close();

        return fileName;
    }

    private static String randomString(int length) {
        char[] charSet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray();
        
        Random random = new SecureRandom();
        char[] result = new char[length];
        for (int i = 0; i < result.length; i++) {
            int randomCharIndex = random.nextInt(charSet.length);
            result[i] = charSet[randomCharIndex];
        }
        return new String(result);
    }
    
    private String generateHash() {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        String text = dateFormat.format(date);

        byte[] digest;
        byte[] buffer = text.getBytes();
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.reset();
            // Envio el buffer el mensaje a encriptar
            md.update(buffer);
            // Obtengo el Digest del Message
            digest = md.digest();
            // Obtengo la cadena del hash en valores hexadecimales
            return toHexadecimal(digest);

        } catch (Exception ex) {
            return null;
        }
    }

    private String toHexadecimal(byte[] digest) {
        String hash = "";
        for (byte aux : digest) {
            int b = aux & 0xff; // Hace un cast del byte a hexadecimal
            if (Integer.toHexString(b).length() == 1) {
                hash += "0";
            }
            hash += Integer.toHexString(b);
        }
        return hash;
    }
}
