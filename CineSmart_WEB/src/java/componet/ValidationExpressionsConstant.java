/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package componet;

/**
 *
 * @author admin
 */
public class ValidationExpressionsConstant {
    private static final String ACENTOS = "ñÑáéíóúÁÉÍÓÚ";
    private static final String ABC_LAT = "A-Za-z" + ACENTOS;
    
    public static final String ENTERO_POSITIVO = "([0-9]{1,8})?";
    
    public static final String PRECIO = "((([0-9]{1,10})(.|,))?[0-9]{1,10})?";
    
    public static final String TEXTO_BASICO = "([" + ABC_LAT + " ]{2,}" + "([!.,;:_\\-" + ABC_LAT + " ]*)?)?";
    
    public static final String TEXTO_ALFA_N = "([" + ABC_LAT + "0-9 ]{2,}" + "([$%!.,;:_\\-" + ABC_LAT + "0-9 ]*)?)?";
    
    public static final String TEXTO_LIMPIO = "([" + ABC_LAT + " ]{2,})?";
    
    public static final String EMAIL = "(^(([^<>()\\[\\]\\\\.,;:\\s@\"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@\"]+)*)|(\".+\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$)?";
    
    public static final String NUMEROS = "([0-9]+)?";
}
