/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package componet;

import entity.SnackOrder;
import model.SnackOrderModel;

/**
 *
 * @author admin
 */
public class SnackOrderConverter {
    
    public SnackOrder convertSnackOrderModelToSnackOrder(SnackOrderModel som) {
        SnackOrder so = new SnackOrder();
        
        so.setAccount(som.getClient());
        so.setOrderDate(som.getOrderDate());
        so.setStatus(som.getStatus());
        so.setTotal(som.getTotal());
        so.setSnackOrdersDetail(som.getSnackOrdersDetail());
        
        return so;
    } 
}
