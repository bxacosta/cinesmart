/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package componet;

import entity.Snack;
import model.SnackModel;

/**
 *
 * @author admin
 */
public class SnackConverter {
    
    public Snack convertSnackModelToSnack(SnackModel snackModel) {
        Snack snack = new Snack();
        
        snack.setName(snackModel.getName());
        snack.setCost(snackModel.getCost());
        snack.setDescription(snackModel.getDescription());
        snack.setImageName(snackModel.getImageName());
        snack.setImageData(snackModel.getImageData());
        
        return snack;
    }
}
