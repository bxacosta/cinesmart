/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package componet;

import entity.Account;
import model.AccountModel;

/**
 *
 * @author admin
 */
public class AccountConverter {

    public Account convertAccountModelToAccount(AccountModel am) {
        Account a = new Account();
        a.setUsername(am.getEmail());
        a.setEnable(am.isEnable());
        a.setPerson(am.getPerson());
        a.setRole(am.getRole());
        
        return a;
    }
    
    public Account convertAccountModelToAccount(AccountModel am, Account a) {
        a.setEnable(am.isEnable());
        a.setPerson(am.getPerson());
        a.setRole(am.getRole());
        
        return a;
    }
    
    public AccountModel convertAccountToAccountModel(Account a) {
        AccountModel am = new AccountModel();
        
        am.setId(a.getId());
        am.setEmail(a.getUsername());
        am.setEnable(a.getEnable());
        am.setPerson(a.getPerson());
        am.setRole(a.getRole());
        
        return am;
    }
}
