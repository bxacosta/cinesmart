/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package componet;

import java.io.File;

/**
 *
 * @author admin
 */
public class AppConstant {
    public static final String ADMIN_ROLE = "admin";
    public static final String USER_ROLE = "user";
    
    public static final String MOVIE_IMAGE_PATH = "resources" + File.separator + "images" + File.separator + "movie" + File.separator;
    public static final String SNACK_IMAGE_PATH = "resources" + File.separator + "images" + File.separator + "snack" + File.separator;
    public static final String HALL_IMAGE_PATH = "resources" + File.separator + "images" + File.separator + "hall" + File.separator;
    
    public static final String DEMO_MOVIE_IMAGE_PATH = "resources" + File.separator + "images" + File.separator + "demo" + File.separator + "movie" + File.separator;
    public static final String DEMO_SNACK_IMAGE_PATH = "resources" + File.separator + "images" + File.separator + "demo" + File.separator + "snack" + File.separator;
    public static final String DEMO_HALL_IMAGE_PATH = "resources" + File.separator + "images" + File.separator + "demo" + File.separator + "hall" + File.separator;
    
}
