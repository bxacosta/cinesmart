/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package componet;

import entity.FunctionOrder;
import model.FunctionOrderModel;

/**
 *
 * @author admin
 */
public class FunctionOrderConverter {
    
    public FunctionOrder convertFunctionOrderModelToFunctionOrder(FunctionOrderModel fom) {
        FunctionOrder fo = new FunctionOrder();
        
        fo.setAccount(fom.getClient());
        fo.setOrderDate(fom.getOrderDate());
        fo.setStatus(fom.getStatus());
        fo.setTotal(fom.getTotal());
        fo.setFunctionOrdersDetail(fom.getOrdersDetail());
        
        
        return fo;
    }
}
